package de.bloodgames.fighttheboss.gamemanagement.gamephases;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.Countdown;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.io.IOException;

/**
 * Created by André on 30.11.2017.
 */
public class EndPhase extends GamePhase {

    public EndPhase( ) {

        super( "endPhase" );
    }

    @Override
    public void init( ) {

        initConfig();

        setCountdown( new Countdown( 21, 0, this ) {
            @Override
            public void onFinish( ) {

                Bukkit.getOnlinePlayers().forEach( player->getUserFactory().getUser( player ).sendToLobby() );

                getGameFactory().endGame();
                getMessageHandler()
                        .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.end" ) );
            }
        } );

        getCountdown().addEvent( ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ), 20, 15 );

        getCountdown().addEventRange( 10, 1, ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ) );

        return;
    }

    @Override
    public void initConfig( ) {

        getConfig().addDefaultOption( "game.countdown.message.remain", "&cDer Server startet in &4<time> &cSekunden neu..." );
        getConfig().addDefaultOption( "game.countdown.message.end", "&cDer Server startet neu..." );

        try {
            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Override public void start( ) {

        super.start();
        Bukkit.getOnlinePlayers().forEach( player->{

            User user = getUserFactory().getUser( player );

            user.getPlayer().getInventory().clear();
            user.setSpectator( false );
            user.getScoreboard().unregisterScoreboard();

            if ( getSpawnFactory().getSpawnById( "end.spawn" ) != null ) {

                getSpawnFactory().getSpawnById( "end.spawn" ).teleportUser( user );
            } else {

                MessageHandler.debug( "Spawn (<spawn>) -> is not set yet.", "spawn", "end.spawn" );
            }

            long oldPoints = user.getStats().getCachedStatsTypesIntegerMap().get( Stats.StatsTypes.POINTS );
            long newPoints = user.getStats().getStat( Stats.StatsTypes.POINTS );

            if ( (newPoints-oldPoints > 0) ) {

                user.sendMessage( getGameFactory().getConfig().getString( "game.messages.addedPoints" ), "points", Long.toString((newPoints-oldPoints)) );
            }
        } );
    }

    @Override
    public void stopPhase( ) {

        super.stopPhase();

        getGameFactory().endGame();
    }

    @Override
    public void onPlayerJoin( Player player, PlayerJoinEvent event ) {

        player.setGameMode( GameMode.SURVIVAL );
    }

    @Override
    public void onPlayerLeave( Player player, PlayerQuitEvent event ) {

    }

    @Override
    public void onPlayerChat( Player player, AsyncPlayerChatEvent event ) {

        User user = FightTheBoss.getInstance().getUserFactory().getUser( player );
        getMessageHandler()
                .sendMessage( MessageHandler.MessageType.USER, user, this.getClass(), event.getMessage() );
    }

    @Override public void onPlayerDeath( Player player, PlayerDeathEvent event ) {

    }

    @Override public void onPlayerRespawn( Player player, PlayerRespawnEvent event ) {

    }


}
