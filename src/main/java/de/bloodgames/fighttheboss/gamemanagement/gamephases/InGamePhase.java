package de.bloodgames.fighttheboss.gamemanagement.gamephases;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.Countdown;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.listener.InGamePhaseListener;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by André on 30.11.2017.
 */
public class InGamePhase extends GamePhase {


    @Getter
    private InGamePhaseListener listener = new InGamePhaseListener( this );

    @Getter
    private List < Location > chestLocations = new ArrayList <>();

    public InGamePhase( ) {

        super( "inGamePhase" );
    }

    @Override
    public void init( ) {

        initConfig();

        setNextGamePhase( FightTheBoss.getInstance().getDeathMatchPhase() );
        setShowConnectionMessages( false );

        setCountdown( new Countdown( getConfig().getInt( "game.countdown.startValue" ), 0, this ) {
            @Override public void onFinish( ) {

                if ( getGameFactory().getWinningTeam() == null ) {

                    getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig()
                            .getString( "game.messages.countdown.end" ) );

                } else {

                    setNextGamePhase( FightTheBoss.getInstance().getEndPhase() );

                    getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig()
                            .getString( "game.messages.win" ), "team", getGameFactory().getWinningTeam()
                            .getDisplayName() );

                    getGameFactory().getWinningTeam()
                            .getUserList()
                            .forEach( user->user.getStats().addStat( Stats.StatsTypes.WINS, 1 ).addStat( Stats.StatsTypes.POINTS, 100 ) );
                    if ( getGameFactory().getWinningTeam().getId().contains( "team1" ) ) {

                        getTeamHandler().getTeamById( "team2" )
                                .getDeadUserList()
                                .forEach( user->user.getStats().addStat( Stats.StatsTypes.LOSES, 1 ) );
                    } else {

                        getTeamHandler().getTeamById( "team1" )
                                .getDeadUserList()
                                .forEach( user->user.getStats().addStat( Stats.StatsTypes.LOSES, 1 ) );
                    }
                }

                stopPhase();
            }
        } );

        getCountdown().addEvent( new Countdown.CountdownEvent() {
            @Override public void run( ) {

                getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.messages.countdown.remain.minutes" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() / 60 ) );
            }
        }, 5 * 60, ( int ) 2.5 * 60, 60 );

        getCountdown().addEvent( new Countdown.CountdownEvent() {
            @Override public void run( ) {

                getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.messages.countdown.remain.seconds" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) );
            }
        }, 30, 15 );

        getCountdown().addEventRange( 10, 1, new Countdown.CountdownEvent() {
            @Override public void run( ) {

                getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.messages.countdown.remain.seconds" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) );
            }
        } );

        getCountdown().addEventRange(getCountdown().getStartValue(), getCountdown().getEndValue(), ( )->{

            if ( getCountdown().getCurrentValue() % 60 == 0 ) {

                if ( getChestLocations().size() == 0 ) {
                    return;
                }

                new BukkitRunnable() {
                    @Override public void run( ) {

                        getChestLocations().forEach( location -> {
                            if ( location.getBlock() == null || location.getBlock().getType() != Material.CHEST ) {

                                location.getBlock().setType( Material.CHEST );
                                location.getBlock().getState().update();
                            }
                        } );
                    }
                }.runTask(getFightTheBoss());
            }
        } );

        getSpawnFactory().copySpawnsFromExistingGamePhase( FightTheBoss.getInstance().getStartPhase() );
    }

    @Override
    public void initConfig( ) {

        getConfig().addDefaultOption( "game.countdown.startValue", 15 * 60 );
        getConfig().addDefaultOption( "game.messages.countdown.remain.minutes", "&7Die Runde endet in &c<time> &7Minute(n)." );
        getConfig().addDefaultOption( "game.messages.countdown.remain.seconds", "&7Die Runde endet in &4<time> &7Sekunden." );
        getConfig().addDefaultOption( "game.messages.countdown.end", "&cDie Runde endet...." );
        getConfig().addDefaultOption( "game.messages.win", "&7Das Team &c<team> &7hat gewonnen!" );
        getConfig().addDefaultOption( "game.messages.noWinner", "&cKein Team hat gewonnen!" );
        getConfig().addDefaultOption( "game.messages.boss.death", "&4Der Boss von Team <team> &4ist gestorben!" );
        getConfig().addDefaultOption( "game.messages.boss.ownBoss", "&4Du kannst deinen eigenen Boss nicht angreifen!" );
        getConfig().addDefaultOption( "game.messages.user.death", "&7<user> &7ist gestorben!" );
        getConfig().addDefaultOption( "game.messages.user.deathByUser", "&7<user> &7wurde von <killer> &7get<oe>tet!" );
        getConfig().addDefaultOption( "game.messages.team.eliminated", "&4Das Team <team> &4wurde augeschaltet!" );

        try {
            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void start( ) {

        super.start();
        getGameFactory().setAllowedToMove( true );

        Bukkit.getPluginManager().registerEvents( getListener(), FightTheBoss.getInstance() );
    }

    @Override public void stopPhase( ) {

        super.stopPhase();
        if ( getChestLocations().size() != 0 ) {
            getChestLocations().forEach( location->
                    location.getWorld().getBlockAt( location ).setType( Material.CHEST ));
        }

        HandlerList.unregisterAll( getListener() );
    }

    @Override
    public void onPlayerJoin( Player player, PlayerJoinEvent event ) {

    }

    @Override
    public void onPlayerLeave( Player player, PlayerQuitEvent event ) {

        User user = getUserFactory().getUser( player );
        if ( user.isSpectator() ) {

            return;
        }

        Team team = user.getTeam();
        if ( team.getTeamSize() != 1 ) {
            return;
        }

        getGameFactory().setWinningTeam( team.getId().equals( "team1" ) ? getTeamHandler()
                .getTeamById( "team2" ) : getTeamHandler().getTeamById( "team1" ) );
        getCountdown().onFinish();
    }

    @Override
    public void onPlayerChat( Player player, AsyncPlayerChatEvent event ) {

        User user = FightTheBoss.getInstance().getUserFactory().getUser( player );

        if ( user.isSpectator() ) {

            String finalMessage = MessageHandler.getMessage( "&7" + user.getUsername() + "&7: &f" + event.getMessage() );

            Bukkit.getOnlinePlayers().forEach( player1 -> {

                User user1 = getUserFactory().getUser( player1 );
                if ( user1.isSpectator() ) {

                    user1.sendMessage( finalMessage );
                }
            } );
            event.setCancelled( true );
            return;
        }

        if ( event.getMessage().startsWith( "@a" ) ) {
            String finalMessage = MessageHandler
                    .getMessage( "&7[&e@&7] " + user.getTeam().getColorCode() + "<player>&7: &f" + event.getMessage()
                            .substring( 2, event.getMessage().length() ), "player", user.getUsername() );
            event.setFormat( finalMessage );
            event.setCancelled( false );
            return;
        }

        String finalMessage = MessageHandler.getMessage( "&7[" + user.getTeam()
                .getDisplayName() + "§7] " + user.getUsername() + "&7: &f" + event.getMessage() );
        user.getTeam().getUserList().forEach( ( user1 )->user1.sendMessage( finalMessage ) );

    }

    @Override public void onPlayerRespawn( Player player, PlayerRespawnEvent event ) {

        User user = getUserFactory().getUser( player );

        if ( user.getTeam().getBoss().isAlive() ) {

            MessageHandler.debug( "Boss of User (<user>) is alive!", "user", user.getUsername() );
            Location location = getRandomSpawn( user ).getLocation();
            event.setRespawnLocation( location );

            return;
        }

        MessageHandler.debug( "Boss of User (<user>) is not alive!", "user", user.getUsername() );

        user.setSpectator( true );
        Location location = getSpawnFactory().getSpawnById( "spectator.spawn" ).getLocation();
        event.setRespawnLocation( location );
    }

    @Override public void onPlayerDeath( Player player, PlayerDeathEvent event ) {

        User user = getUserFactory().getUser( player );
        User killer;

        user.getStats().addStat( Stats.StatsTypes.DEATHS, 1 );

        if ( user.getPlayer().getKiller() == null ) {

            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.user.death" ), "user", user
                    .getUsername() );
        } else {

            killer = getUserFactory().getUser( user.getPlayer().getKiller() );
            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.user.deathByUser" ), "user", user
                    .getUsername(), "killer", killer.getUsername() );
            killer.getStats().addStat( Stats.StatsTypes.KILLS, 1 ).addStat( Stats.StatsTypes.POINTS, 20 );
        }

        event.setDeathMessage( null );

        if ( !user.getTeam().getBoss().isAlive() && user.getTeam().getUserList().size() == 1 ) {

            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.team.eliminated" ), "team", user
                    .getTeam()
                    .getDisplayName() );

            Bukkit.getScheduler().scheduleSyncDelayedTask( FightTheBoss.getInstance(), ( )->{

                getGameFactory().setWinningTeam( user.getTeam()
                        .getId()
                        .contains( "1" ) ? getTeamHandler().getTeamById( "team2" ) : getTeamHandler().getTeamById( "team1" ) );
                getCountdown().onFinish();
            }, 10 );
        }
    }
}
