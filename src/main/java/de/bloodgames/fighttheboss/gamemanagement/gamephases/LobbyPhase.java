package de.bloodgames.fighttheboss.gamemanagement.gamephases;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.Countdown;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.spawnmanagement.Spawn;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.inventory.InventoryGui;
import de.bloodgames.fighttheboss.utils.items.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;

/**
 * Created by André on 29.11.2017.
 */
public class LobbyPhase extends GamePhase {

    @Getter @Setter
    private int startValue, endValue;

    @Getter
    private Spawn spawnLocation;


    public LobbyPhase( ) {

        super( "lobbyPhase" );
    }

    @Override
    public void init( ) {

        initConfig();

        setNextGamePhase( getFightTheBoss().getStartPhase() );

        setCountdown( new Countdown( startValue, endValue, this ) {
            @Override
            public void onFinish( ) {

                if ( Bukkit.getOnlinePlayers().size() < getFightTheBoss().getGameFactory().getMinPlayers() ) {
                    getCountdown().reset();
                    getMessageHandler()
                            .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.restart" ) );

                    return;
                }

                getMessageHandler()
                        .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.end" ) );
                stopPhase();

                for ( User user : getGameFactory().getInGameUsers() ) {
                    user.getInventoryGuiList().forEach( InventoryGui:: destroy );
                }
            }
        } );

        getCountdown().addEvent( ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ), 60, 30, 15, 10 );

        getCountdown().addEventRange( 5, 1, ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ) );

        if ( !getSpawnFactory().getSpawnList().isEmpty() ) {

            spawnLocation = getSpawnFactory().getSpawnList().get( 0 );
        }
    }

    @Override
    public void initConfig( ) {

        getConfig().addDefaultOption( "countdown.startValue", 60 );
        getConfig().addDefaultOption( "countdown.endValue", 0 );
        getConfig().addDefaultOption( "game.messages.join", "§8<user> &7hat das Spiel betreten." );
        getConfig().addDefaultOption( "game.messages.quit", "§8<user> &7hat das Spiel verlassen." );
        getConfig().addDefaultOption( "game.countdown.message.remain", "&cDas Spiel startet in &7<time>&c Sekunden." );
        getConfig().addDefaultOption( "game.countdown.message.end", "&cDas Spiel startet.." );
        getConfig().addDefaultOption( "game.countdown.message.restart", "&4Es sind noch zu wenig Spieler da.." );
        getConfig().addDefaultOption( "game.team.message.chosen", "&7Du hast das Team <team> &7ausgew<ae>hlt." );
        getConfig().addDefaultOption( "items.teamSelection.name", "&6Teamauswahl" );
        getConfig().addDefaultOption( "items.teamSelection.lore", "Hiermit kannst du ein Team auswählen." );
        getConfig().addDefaultOption( "items.team.team1.name", "&9Blau" );
        getConfig().addDefaultOption( "items.team.team1.lore", "Wähle das Team Blau." );
        getConfig().addDefaultOption( "items.team.team2.name", "&cRot" );
        getConfig().addDefaultOption( "items.team.team2.lore", "Wähle das Team Rot." );
        getConfig().addDefaultOption( "items.team.selected", "&7Du hast dieses Team gewählt." );
        getConfig().addDefaultOption( "menu.teamSelection.title", "&6Teamauswahl" );

        try {
            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        setStartValue( getConfig().getInt( "countdown.startValue" ) );
        setEndValue( getConfig().getInt( "countdown.endValue" ) );
    }

    @Override
    public void start( ) {

        super.start();

        getGameFactory().setAllowedToMove( true );
    }

    @Override
    public void stopPhase( ) {

        super.stopPhase();

        getGameFactory().getInGameUsers().forEach( user->{

            user.getInventoryGuiList().forEach( inventoryGui->inventoryGui.destroy() );
            user.getInventoryGuiList().clear();
        } );
    }

    @Override
    public void onPlayerJoin( Player player, PlayerJoinEvent event ) {

        player.setGameMode( GameMode.SURVIVAL );

        if ( getSpawnLocation() != null ) {

            getSpawnLocation().teleportUser( event.getPlayer() );
        }

        getGameFactory().getInGameUsers().add( getUserFactory().getUser( player ) );

        if ( getGameFactory().getIngameUserAmount() == 1 ) {

            getCountdown().startCountdown();
        }

        ItemStack startItemStack = new ItemBuilder( Material.EMERALD ).setName( getConfig().getString( "items.teamSelection.name" ) )
                .addLore( getConfig().getString( "items.teamSelection.lore" ) )
                .hideFlags()
                .build();
        player.getInventory().setItem( 0, startItemStack );
        player.updateInventory();

        ItemStack team1ItemStack = new ItemBuilder( Material.LEATHER_BOOTS ).setColor( Color.BLUE )
                .setName( getConfig().getString( "items.team.team1.name" ) )
                .addLore( getConfig().getString( "items.team.team1.lore" ) ).hideFlags().build();

        ItemStack team2ItemStack = new ItemBuilder( Material.LEATHER_BOOTS ).setColor( Color.RED )
                .setName( getConfig().getString( "items.team.team2.name" ) )
                .addLore( getConfig().getString( "items.team.team2.lore" ) ).hideFlags().build();

        new InventoryGui( getConfig().getString( "menu.teamSelection.title" ), 9, startItemStack, player ) {

            @Override
            public void addItems( ) {

                getInventory().setItem( 0, team1ItemStack.clone() );
                getInventory().setItem( 1, team2ItemStack.clone() );
            }

            @Override
            public void onClick( Player player, InventoryClickEvent clickEvent ) {

                ItemStack clickedItem = clickEvent.getCurrentItem();
                if ( clickedItem == null || clickedItem.getType().equals( Material.AIR ) ) {
                    return;
                }

                User user = FightTheBoss.getInstance()
                        .getUserFactory()
                        .getUser( player );

                if ( clickEvent.getCurrentItem().equals( team1ItemStack ) ) {

                    getTeamHandler()
                            .selectTeam( user, getFightTheBoss().getTeamHandler().getTeamById( "team1" ) );

                    user.sendMessage( MessageHandler.MessageType.GAME, getConfig().getString( "game.team.message.chosen" ), "team", getTeamHandler()
                            .getTeamById( "team1" )
                            .getDisplayName() );

                }

                if ( clickEvent.getCurrentItem().equals( team2ItemStack ) ) {

                    FightTheBoss.getInstance()
                            .getTeamHandler()
                            .selectTeam( user, getTeamHandler().getTeamById( "team2" ) );

                    user.sendMessage( MessageHandler.MessageType.GAME, getConfig().getString( "game.team.message.chosen" ), "team", getTeamHandler()
                            .getTeamById( "team2" )
                            .getDisplayName() );
                }

                closeGui();
            }

            @Override public void updateItems( ) {

                if ( getUserFactory().getUser( player ).getTeam() == null ) {

                    return;
                }

                if ( getUserFactory().getUser( player ).getTeam().getId().equals( "team1" ) ) {

                    getInventory().clear();

                    getInventory().setItem( 0, new ItemBuilder().fromItemStack( team1ItemStack.clone() )
                            .addLore( getConfig().getString( "items.team.selected" ) )
                            .addEffectToGlow()
                            .build() );
                    getInventory().setItem( 1, team2ItemStack.clone() );
                } else {

                    getInventory().clear();

                    getInventory().setItem( 0, team1ItemStack.clone() );
                    getInventory().setItem( 1, new ItemBuilder().fromItemStack( team2ItemStack.clone() )
                            .addLore( getConfig().getString( "items.team.selected" ) )
                            .addEffectToGlow()
                            .build() );
                }
            }
        };
    }

    @Override
    public void onPlayerLeave( Player player, PlayerQuitEvent event ) {

        if ( getUserFactory().getUser( player ).getInventoryGuiList().size() != 0 ) {

            getUserFactory().getUser( player ).getInventoryGuiList().forEach( inventoryGui->inventoryGui.destroy() );
        }
    }

    @Override
    public void onPlayerChat( Player player, AsyncPlayerChatEvent event ) {

        User user = getFightTheBoss().getUserFactory().getUser( player );
        getMessageHandler()
                .sendMessage( MessageHandler.MessageType.USER, user, this.getClass(), event.getMessage() );
    }

    @Override public void onPlayerDeath( Player player, PlayerDeathEvent event ) {

    }

    @Override public void onPlayerRespawn( Player player, PlayerRespawnEvent event ) {

    }
}
