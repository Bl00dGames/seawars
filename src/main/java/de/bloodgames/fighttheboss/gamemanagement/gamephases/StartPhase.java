package de.bloodgames.fighttheboss.gamemanagement.gamephases;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.Countdown;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.io.IOException;

/**
 * Created by André on 30.11.2017.
 */
public class StartPhase extends GamePhase {

    public StartPhase( ) {

        super( "startPhase" );
    }

    @Override
    public void init( ) {

        initConfig();

        setShowConnectionMessages( false );

        setCountdown( new Countdown( 30, 0, this ) {
            @Override
            public void onFinish( ) {

                getMessageHandler()
                        .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.end" ) );
                stopPhase();
            }
        } );

        getCountdown().addEvent( ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ), 60, 30, 15 );

        getCountdown().addEventRange( 10, 1, ( )->getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.remain" ), "time", String
                        .valueOf( getCountdown().getCurrentValue() ) ) );

    }

    @Override
    public void initConfig( ) {

        getConfig().addDefaultOption( "game.countdown.message.remain", "Das Spiel beginnt in &6<time>&7 Sekunden." );
        getConfig().addDefaultOption( "game.countdown.message.end", "Das Spiel beginnt." );
        getConfig().addDefaultOption( "game.countdown.message.restart", "&cSpiel neustarten, zu wenig Spieler..." );

        try {
            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPlayerJoin( Player player, PlayerJoinEvent event ) {

    }

    @Override
    public void start( ) {

        super.start();
        setNextGamePhase( getFightTheBoss().getInGamePhase() );

        getGameFactory().setAllowedToMove( true );

        getTeamHandler().autoSelect();

        getGameFactory().getInGameUsers().forEach( user->{

            user.getStats().addStat( Stats.StatsTypes.GAMESPLAYED, 1 );
            user.teleport( getRandomSpawn( user ) );
            user.getPlayer().getInventory().clear();
            user.setScoreboard();
        } );

        getGameFactory().setAllowedToMove( false );

        Bukkit.getWorlds().forEach( world->world.getEntities().forEach( entity->{

            if ( !( entity instanceof Player ) ) {

                entity.remove();
            }
        } ) );

        getTeamHandler().getTeamList().forEach( team->team.spawnBoss( team.getBossSpawn() ) );
    }

    @Override
    public void onPlayerLeave( Player player, PlayerQuitEvent event ) {

        if ( getGameFactory().getInGameUsers().size() < getGameFactory().getMinPlayers() ) {
            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.countdown.message.restart" ) );
            setNextGamePhase( FightTheBoss.getInstance().getEndPhase() );
            stopPhase();
        }
    }

    @Override
    public void onPlayerChat( Player player, AsyncPlayerChatEvent event ) {

        User user = FightTheBoss.getInstance().getUserFactory().getUser( player );

        if ( event.getMessage().startsWith( "@a" ) ) {
            String finalMessage = MessageHandler
                    .getMessage( "&7[&e@&7] " + user.getTeam().getColorCode() + "<player>&7: &f" + event.getMessage()
                            .substring( 2, event.getMessage().length() ), "player", user.getUsername() );
            event.setFormat( finalMessage );
            event.setCancelled( false );
            return;
        }

        String finalMessage = MessageHandler.getMessage( "&7[" + user.getTeam()
                .getDisplayName() + "§7] " + user.getUsername() + "&7: &f" + event.getMessage() );
        user.getTeam().getUserList().forEach( ( user1 )->user1.sendMessage( finalMessage ) );
    }

    @Override public void onPlayerDeath( Player player, PlayerDeathEvent event ) {

    }

    @Override public void onPlayerRespawn( Player player, PlayerRespawnEvent event ) {

    }
}
