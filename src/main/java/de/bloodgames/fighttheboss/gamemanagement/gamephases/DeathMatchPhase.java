package de.bloodgames.fighttheboss.gamemanagement.gamephases;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.Countdown;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.io.IOException;

public class DeathMatchPhase extends GamePhase {

    public DeathMatchPhase( ) {

        super( "deathMatchPhase" );
    }

    @Override public void init( ) {

        initConfig();

        setNextGamePhase( FightTheBoss.getInstance().getEndPhase() );

        setCountdown( new Countdown( getConfig().getInt( "game.countdown.startValue" ), 0, this ) {
            @Override public void onFinish( ) {

                if ( getGameFactory().getWinningTeam() != null ) {

                    setNextGamePhase( FightTheBoss.getInstance().getEndPhase() );

                    getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig()
                            .getString( "game.messages.win" ), "team", getGameFactory().getWinningTeam()
                            .getDisplayName() );

                    getGameFactory().getWinningTeam()
                            .getUserList()
                            .forEach( user->user.getStats().addStat( Stats.StatsTypes.WINS, 1 ).addStat( Stats.StatsTypes.POINTS, 100 ) );
                    if ( getGameFactory().getWinningTeam().getId().contains( "team1" ) ) {

                        getTeamHandler().getTeamById( "team2" )
                                .getDeadUserList()
                                .forEach( user->user.getStats().addStat( Stats.StatsTypes.LOSES, 1 ) );
                    } else {

                        getTeamHandler().getTeamById( "team1" )
                                .getDeadUserList()
                                .forEach( user->user.getStats().addStat( Stats.StatsTypes.LOSES, 1 ) );
                    }
                }

                getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig()
                        .getString( "game.messages.countdown.end" ) );

                stopPhase();
            }
        } );

    }

    @Override public void initConfig( ) {

        getConfig().addDefaultOption( "game.countdown.startValue", 15 * 60 );
        getConfig().addDefaultOption( "game.messages.countdown.remain.minutes", "&7Das DeathMatch endet in &c<time> &7Minute(n)." );
        getConfig().addDefaultOption( "game.messages.countdown.remain.seconds", "&Das DeathMatch endet in &4<time> &7Sekunden." );
        getConfig().addDefaultOption( "game.messages.countdown.end", "&cDie DeathMatch endet...." );
        getConfig().addDefaultOption( "game.messages.win", "&7Das Team &c<team> &7hat gewonnen!" );
        getConfig().addDefaultOption( "game.messages.noWinner", "&cKein Team hat gewonnen!" );
        getConfig().addDefaultOption( "game.messages.user.death", "&7<user> &7ist gestorben!" );
        getConfig().addDefaultOption( "game.messages.user.deathByUser", "&7<user> &7wurde von <killer> &7get<oe>tet!" );
        getConfig().addDefaultOption( "game.messages.team.eliminated", "&4Das Team <team> &4wurde augeschaltet!" );

        try {
            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Override public void start( ) {

        super.start();

        FightTheBoss.getInstance().getGameFactory().getInGameUsers().forEach( user->{

            user.teleport( getRandomSpawn( user ) );
        } );

        Bukkit.getWorlds().forEach( world->world.getEntities().forEach( entity->{
            if ( !( entity instanceof Player ) ) {
                entity.remove();
            }
        } ) );
    }

    @Override public void onPlayerJoin( Player player, PlayerJoinEvent event ) {

    }

    @Override public void onPlayerLeave( Player player, PlayerQuitEvent event ) {

        User user = getUserFactory().getUser( player );
        if ( user.isSpectator() ) {

            return;
        }

        Team team = user.getTeam();
        if ( team.getTeamSize() != 1 ) {
            return;
        }

        getGameFactory().setWinningTeam( team.getId().equals( "team1" ) ? getTeamHandler()
                .getTeamById( "team2" ) : getTeamHandler().getTeamById( "team1" ) );
        getCountdown().onFinish();
    }

    @Override public void onPlayerChat( Player player, AsyncPlayerChatEvent event ) {

    }

    @Override public void onPlayerDeath( Player player, PlayerDeathEvent event ) {

        User user = getUserFactory().getUser( player );
        User killer;

        user.getStats().addStat( Stats.StatsTypes.DEATHS, 1 );

        if ( user.getPlayer().getKiller() == null ) {

            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.user.death" ), "user", user
                    .getUsername() );
        } else {

            killer = getUserFactory().getUser( user.getPlayer().getKiller() );
            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.user.deathByUser" ), "user", user
                    .getUsername(), "killer", killer.getUsername() );
            killer.getStats().addStat( Stats.StatsTypes.KILLS, 1 ).addStat( Stats.StatsTypes.POINTS, 20 );
        }

        event.setDeathMessage( null );

        if ( user.getTeam().getUserList().size() == 1 ) {

            getMessageHandler().sendMessage( MessageHandler.MessageType.GAME, null, getClass(), getConfig().getString( "game.messages.team.eliminated" ), "team", user
                    .getTeam()
                    .getDisplayName() );

            Bukkit.getScheduler().scheduleSyncDelayedTask( FightTheBoss.getInstance(), ( )->{

                getGameFactory().setWinningTeam( user.getTeam()
                        .getId()
                        .contains( "1" ) ? getTeamHandler().getTeamById( "team2" ) : getTeamHandler().getTeamById( "team1" ) );
                getCountdown().onFinish();
            }, 30 );
        }
    }

    @Override public void onPlayerRespawn( Player player, PlayerRespawnEvent event ) {

        User user = getUserFactory().getUser( player );

        user.setSpectator( true );
        Location location = getSpawnFactory().getSpawnById( "spectator.spawn" ).getLocation();
        event.setRespawnLocation( location );
    }
}
