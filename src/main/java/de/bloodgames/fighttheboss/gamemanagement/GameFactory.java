package de.bloodgames.fighttheboss.gamemanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.boss.CustomZombie;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.Config;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.items.NMSUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by André on 29.11.2017.
 */
public class GameFactory {

    @Getter
    private final Config config = new Config( "plugins/FightTheBoss/configs", "FightTheBoss" );
    @Getter
    private int minPlayers, maxPlayers, teamSize, teamAmount;
    @Getter
    private String prefix;
    @Getter
    @Setter
    private GamePhase currentGamePhase;
    @Getter
    @Setter
    private boolean inGame = false, end = false, allowedToMove = true, pvp = false, debug = false;
    @Getter
    private List < GamePhase > gamePhaseList = new ArrayList <>();
    @Getter
    private List < User > inGameUsers = new ArrayList <>(), spectators = new ArrayList <>();

    @Getter @Setter
    private Team winningTeam;

    @SuppressWarnings( "unchecked" )
    public GameFactory( ) {

        //Init config
        getConfig().addDefaultOption( "mysql.username", "username" );
        getConfig().addDefaultOption( "mysql.password", "password" );
        getConfig().addDefaultOption( "mysql.host", "host" );
        getConfig().addDefaultOption( "mysql.database", "database" );
        getConfig().addDefaultOption( "mysql.port", 3306 );
        getConfig().addDefaultOption( "game.minPlayers", 4 );
        getConfig().addDefaultOption( "game.maxPlayers", 10 );
        getConfig().addDefaultOption( "game.teamSize", 5 );
        getConfig().addDefaultOption( "game.teamAmount", 2 );
        getConfig().addDefaultOption( "game.prefix", "FtB" );
        getConfig().addDefaultOption( "game.debug", false );
        getConfig().addDefaultOption( "game.messages.commands.noPermission", "&cDu hast keine Berechtigung diesen Befehl auszuführen!" );
        getConfig().addDefaultOption( "game.messages.addedPoints", "&e+&7<points> &ePoints" );
        getConfig().addDefaultOption( "game.messages.location.saved", "&7GamePhase (&c<phase>&7) -> Saved Spawn: &c<spawn>" );
        getConfig().addDefaultOption( "game.messages.help", "&7Hilfe:" +
                "<ng> &a/ftb &7| &aRufe diese Seite auf." +
                "<ng> &a/ftb next &7| &aGehe zur n<ae>chsten GamePhase." +
                "<ng> &a/ftb setspawn [gamePhase] [spawnId] &7| &aSetzte einen Spawn-Punkt." +
                "<ng> --------------------------------------" +
                "<ng> &aSpawns welche gesetzt werden m<ue>ssen:" +
                "<ng> &4LobbyPhase:" +
                "<ng> &7 - lobby.spawn" +
                "<ng> &4StartPhase:" +
                "<ng> &7 - teamId.spawnId" +
                "<ng> &7 - spectator.spawn" +
                "<ng> &4InGamePhase:" +
                "<ng> &7 - Wird von StartPhase kopiert" +
                "<ng> &4DeathMatchPhase:" +
                "<ng> &7 - teamId.spawnId" +
                "<ng> &7 - spectator.spawn" +
                "<ng> &4EndPhase:" +
                "<ng> &7 - end.spawn" +
                "<ng> --------------------------------------" );
        getConfig().addDefaultOption( "game.messages.spawn.added", "&7GamePhase (<phase>) -> Spawn (<spawn>) hinzugef<ue>gt." );
        getConfig().addDefaultOption( "game.messages.spawn.error", "&4ERROR&7: GamePhase (<phase>) &4X &7Spawn (<spawn>) nicht hinzugef<ue>gt." );
        getConfig().addDefaultOption( "game.messages.gamePhase.skip", "&7GamePhase (<phase>) <ue>bersprungen." );
        getConfig().addDefaultOption( "game.messages.teamHandler.team.isFull", "&7Team <team> ist bereits voll." );
        getConfig().addDefaultOption( "game.messages.coins.add", "&7Du hast &e<coins> &7erhalten!" );
        getConfig().addDefaultOption( "game.messages.coins.boost", "&7Du hast &e<coins> &7erhalten durch den Boost von &f<player>&7!" );
        getConfig().addDefaultOption( "game.messages.player.isOffline", "&c<player> &cist nicht online!" );
        getConfig().addDefaultOption( "game.messages.stats", "--------------------------------------" +
                "<ng> &7Stats von: &c<user>&7:" +
                "<ng> &7- Kills: &c<kills>" +
                "<ng> &7- Tode: &c<deaths>" +
                "<ng> &7- BossKills: &c<bossKills>" +
                "<ng> &7- Gewonnene Spiele: &c<wins>" +
                "<ng> &7- Verlorene Spiele: &c<loses>" +
                "<ng> &7- Gespielte Spiele: &c<gamesPlayed>" +
                "<ng> &7- Punkte: &c<points>" +
                "<ng> &7- K/D: &c<kd>" +
                "<ng> --------------------------------------" );


        try {

            getConfig().save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        this.minPlayers = getConfig().getInt( "game.minPlayers" );
        this.maxPlayers = getConfig().getInt( "game.maxPlayers" );
        this.teamSize = getConfig().getInt( "game.teamSize" );
        this.teamAmount = getConfig().getInt( "game.teamAmount" );

        this.prefix = getConfig().getString( "game.prefix" );

        this.debug = getConfig().getBoolean( "game.debug" );

        NMSUtils.registerEntity( "Zombie", NMSUtils.MobType.ZOMBIE, CustomZombie.class, false );
    }

    public GameFactory registerGamePhase( GamePhase gamePhase ) {

        getGamePhaseList().add( gamePhase );

        MessageHandler.debug( "GamePhase (<phase>) -> Registered.", "phase", gamePhase.getId() );

        return this;
    }

    public GameFactory startGame( GamePhase gamePhase ) {

        setCurrentGamePhase( gamePhase );
        getCurrentGamePhase().start();

        MessageHandler.debug( "GameHandler initialized, first GamePhase (<phase>) started.", "phase", gamePhase.getId() );

        return this;
    }

    public GameFactory endGame( ) {

        MessageHandler.debug( "GamePhase (<phase>) -> Ended.", "phase", getCurrentGamePhase().getId() );
        FightTheBoss.getInstance().onDisable();
        return this;
    }

    public GameFactory nextPhase( ) {

        MessageHandler.debug( "GamePhase (<phase>) -> Skipped.", "phase", getCurrentGamePhase().getId() );
        getCurrentGamePhase().stopPhase();

        return this;
    }

    public int getIngameUserAmount( ) {

        return getInGameUsers().size();
    }
}
