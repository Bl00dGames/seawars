package de.bloodgames.fighttheboss.gamemanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.spawnmanagement.Spawn;
import de.bloodgames.fighttheboss.spawnmanagement.SpawnFactory;
import de.bloodgames.fighttheboss.teammanagement.TeamHandler;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.usermanagement.UserFactory;
import de.bloodgames.fighttheboss.utils.Config;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_12_R1.PacketPlayInClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by André on 29.11.2017.
 */
public abstract class GamePhase implements Listener {

    @Getter
    private final String id;
    @Getter
    private final Config config;
    @Getter
    private final FightTheBoss fightTheBoss = FightTheBoss.getInstance();
    @Getter
    private final UserFactory userFactory = getFightTheBoss().getUserFactory();
    @Getter
    private final GameFactory gameFactory = getFightTheBoss().getGameFactory();
    @Getter
    private final TeamHandler teamHandler = getFightTheBoss().getTeamHandler();
    @Getter
    private final MessageHandler messageHandler = getFightTheBoss().getMessageHandler();
    @Getter @Setter
    private Countdown countdown;
    @Getter
    private GamePhase nextGamePhase;
    @Getter @Setter
    private boolean showConnectionMessages = true;
    @Getter
    private SpawnFactory spawnFactory;

    public GamePhase( String id ) {

        this.id = id;
        this.config = new Config( "plugins/FightTheBoss/phases", id );

        //Loading spawns
        this.spawnFactory = new SpawnFactory( this );
        this.spawnFactory.loadSpawns();

        //Register GamePhase
        getGameFactory().registerGamePhase( this );
    }

    public void start( Countdown countdown ) {

        setCountdown( countdown );
        start();
    }

    public void start( ) {


        if ( getGameFactory().getInGameUsers().size() != 0 ) {

            if ( getCountdown() != null ) {

                getCountdown().startCountdown();
            }
        }

        Bukkit.getPluginManager().registerEvents( this, FightTheBoss.getInstance() );
        MessageHandler.debug( "GamePhase: <phase> started!", "phase", getId() );
    }

    public void stopPhase( ) {

        if ( getCountdown() != null ) if ( getCountdown().isRunning() ) getCountdown().stop();

        getGameFactory().getInGameUsers().forEach( user->user.getPlayer().setFoodLevel( 20 ) );

        HandlerList.unregisterAll( this );

        MessageHandler.debug( "GamePhase: <phase> stopped!", "phase", getId() );

        if ( getNextGamePhase() != null ) {
            MessageHandler.debug( "GamePhase: <phase> starting now!", "phase", getNextGamePhase().getId() );
            getGameFactory().setCurrentGamePhase( getNextGamePhase() );
            getNextGamePhase().start();
        }

    }

    public void setNextGamePhase( GamePhase nextGamePhase ) {

        this.nextGamePhase = nextGamePhase;
        MessageHandler.debug( "Next GamePhase -> (<phase>)", "phase", nextGamePhase.getId() );
    }

    public abstract void init( );

    public abstract void initConfig( );

    public abstract void onPlayerJoin( Player player, PlayerJoinEvent event );

    public abstract void onPlayerLeave( Player player, PlayerQuitEvent event );

    public abstract void onPlayerChat( Player player, AsyncPlayerChatEvent event );

    public abstract void onPlayerDeath( Player player, PlayerDeathEvent event );

    public abstract void onPlayerRespawn( Player player, PlayerRespawnEvent event );

    @EventHandler
    public void onPlayerJoin( PlayerJoinEvent event ) {

        Player player = event.getPlayer();

        //Perform default PlayerJoinAction
        event.setJoinMessage( null );

        player.setFoodLevel( 20 );
        player.setHealth( 20.0 );
        player.setLevel( 0 );
        player.getInventory().clear();
        player.updateInventory();

        if ( isShowConnectionMessages() ) {

            getMessageHandler()
                    .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.messages.join" ), "user", player
                            .getName() );
        }

        User user = new User( event.getPlayer() );

        getUserFactory().registerUser( user );

        if ( getGameFactory().getCurrentGamePhase() != FightTheBoss.getInstance()
                .getLobbyPhase() && getGameFactory().getCurrentGamePhase() != FightTheBoss.getInstance()
                .getEndPhase() ) {

            user.setSpectator( true );
        }

        //Perform individual PlayerJoinAction
        onPlayerJoin( player, event );
    }

    @EventHandler
    public void onPlayerLeave( PlayerQuitEvent event ) {

        //Perform individual PlayerLeaveAction
        onPlayerLeave( event.getPlayer(), event );

        User user = getUserFactory().getUser( event.getPlayer() );

        event.setQuitMessage( null );

        //Perform default PlayerLeaveAction
        if ( isShowConnectionMessages() ) {

            getMessageHandler()
                    .sendMessage( MessageHandler.MessageType.GAME, null, this.getClass(), getConfig().getString( "game.messages.quit" ), "user", event
                            .getPlayer()
                            .getName() );
        }

        getUserFactory().unregisterUser( user );

        if ( getGameFactory().getIngameUserAmount() == 0 && this != FightTheBoss.getInstance().getEndPhase() ) {

            if ( getCountdown() == null ) {
                return;
            }
            getCountdown().stop();
        }
    }

    @EventHandler
    public void onPlayerChat( AsyncPlayerChatEvent event ) {

        event.setCancelled( true );
        onPlayerChat( event.getPlayer(), event );
    }

    @EventHandler
    public void onPlayerRespawn( PlayerRespawnEvent event ) {

        User user = getUserFactory().getUser( event.getPlayer() );

        Location nullLocation = new Location( event.getRespawnLocation().getWorld(), 0, 0,0 );

        event.setRespawnLocation( nullLocation );

        MessageHandler.debug( "User (<user>) -> Respawn...", "user", user.getUsername() );

        onPlayerRespawn( event.getPlayer(), event );

        if ( event.getRespawnLocation() == nullLocation ) {

            event.setRespawnLocation( getSpawnFactory().getSpawnList().get( 0 ).getLocation() );

            return;
        }
    }

    @EventHandler
    public void onPlayerDeath( PlayerDeathEvent event ) {

        event.getDrops().clear();
        event.setDroppedExp( 0 );

        User user = getUserFactory().getUser( event.getEntity() );
        User killer;

        onPlayerDeath( event.getEntity(), event );

        Bukkit.getScheduler()
                .scheduleSyncDelayedTask( FightTheBoss.getInstance(), ( )->( ( CraftPlayer ) user.getPlayer() ).getHandle().playerConnection
                        .a( new PacketPlayInClientCommand( PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN ) ) );

    }


    public Spawn getRandomSpawn( User user ) {

        Spawn targetSpawn = null;

        List < Spawn > spawnList = new ArrayList <>();
        getSpawnFactory().getTeamSpawnMap().forEach( ( teamId, spawns )->{

            if ( teamId.equals( user.getTeam().getId() ) ) {

                spawns.forEach( spawn->spawnList.add( spawn ) );
            }
        } );

        if ( getSpawnFactory().getSpawnList().size() == 0 ) {

            user.sendMessage( MessageHandler.MessageType.GAME, "No spawn set yet. Please report in forum." );
            return new Spawn( user.getPlayer().getLocation() );
        }

        //Not in game userSpawn
        if ( getGameFactory().getCurrentGamePhase() == getFightTheBoss().getLobbyPhase() || getGameFactory().getCurrentGamePhase() == getFightTheBoss()
                .getEndPhase() || user.isSpectator() ) {

            return getSpawnFactory().getSpawnList().get( 0 );
        }

        //Random spawn for user who is in game
        if ( spawnList.size() == 0 ) {

            return new Spawn( user.getPlayer().getLocation() );
        }

        Random random = new Random();
        targetSpawn = spawnList.get( random.nextInt( spawnList.size() ) );

        while ( targetSpawn.isUsed() ) {

            targetSpawn = spawnList.get( random.nextInt() );
        }

        return targetSpawn;
    }
}
