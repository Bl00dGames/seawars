package de.bloodgames.fighttheboss.gamemanagement.boss;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.spawnmanagement.Spawn;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Boss implements Listener {

    @Getter
    private final String name;

    @Getter
    private final Spawn spawn;

    @Getter @Setter
    private boolean allowedMovement, alive = true;

    @Getter @Setter
    private CustomZombie zombie;

    @Getter @Setter
    private Team team;

    public Boss( String name, Spawn spawn, Team team ) {

        this.name = name;
        this.spawn = spawn;
        this.team = team;
        MessageHandler.debug( "Boss <name> was initialized!", "name", name );
    }

    public Boss onInitSpawnEntity( ) {

        Location location = getSpawn().getLocation();

        setZombie( new CustomZombie( ( ( CraftWorld ) spawn.getLocation().getWorld() ).getHandle() ));

        getZombie().setLocation( location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch() );
        getZombie().setAbsorptionHearts( 40 );
        getZombie().setCustomName( ChatColor.translateAlternateColorCodes( '&', getName() ) + " §c" + ( getZombie().getHealth() + getZombie()
                .getAbsorptionHearts() ) + " ❤" );
        getZombie().setHeadRotation( location.getYaw() );

        Bukkit.getScheduler().scheduleSyncDelayedTask( FightTheBoss.getInstance(), new Runnable() {
            @Override public void run( ) {

                getZombie().getWorld().addEntity( getZombie() );
            }
        } );

        MessageHandler.debug( "CustomZombie spawned!" );

        movement();

        Bukkit.getPluginManager().registerEvents( this, FightTheBoss.getInstance() );

        return this;
    }

    public Boss movement( ) {

        if ( allowedMovement == true ) {

            return this;
        }

        getZombie().setNoAI( true );

        return this;
    }

    public Boss onDespawn( ) {

        getZombie().killEntity();

        return this;
    }

    @EventHandler
    public void onDeath( EntityDeathEvent event ) {

        if ( event.getEntityType() != getZombie().getBukkitEntity().getType() ) {
            return;
        }

        if ( event.getEntity() != getZombie().getBukkitEntity() ) {
            return;
        }

        event.getDrops().clear();
        event.setDroppedExp( 0 );

        FightTheBoss.getInstance()
                .getMessageHandler()
                .sendMessage( MessageHandler.MessageType.GAME, null, getClass(), FightTheBoss.getInstance()
                        .getGameFactory()
                        .getCurrentGamePhase()
                        .getConfig()
                        .getString( "game.messages.boss.death" ), "team", team.getDisplayName() );

        HandlerList.unregisterAll( this );
        setAlive( false );

        if ( !( event.getEntity().getKiller() instanceof Player ) ) {

            return;
        }

        User user = FightTheBoss.getInstance().getUserFactory().getUser( event.getEntity()
                .getKiller() );
        user.getStats().addStat( Stats.StatsTypes.BOSSKILLS, 1 );
        user.getStats().addStat( Stats.StatsTypes.POINTS, 50 );
    }

    @EventHandler( priority = EventPriority.HIGHEST )
    public void onDamage( EntityDamageByEntityEvent event ) {

        if ( event.getEntity() != getZombie().getBukkitEntity() ) {

            return;
        }

        if ( event.getCause() == EntityDamageEvent.DamageCause.FIRE || event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK ) {
            event.getEntity().setFireTicks( 0 );
            event.setCancelled( true );
        }

        if ( !( event.getDamager() instanceof Player || event.getDamager() instanceof Arrow ) || event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION ) {

            event.setCancelled( true );
            return;
        }

        User user = null;

        if ( event.getDamager() instanceof Arrow ) {

            Arrow arrow = ( ( Arrow ) event.getDamager() );
            user = FightTheBoss.getInstance().getUserFactory().getUser( ( ( Player ) arrow.getShooter() ) );
        } else if ( event.getDamager() instanceof Player ) {

            user = FightTheBoss.getInstance().getUserFactory().getUser( ( ( Player ) event.getDamager() ) );
        }

        if ( user != null ) {

            if ( user.getTeam() == getTeam() ) {

                event.setCancelled( true );
                user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                        .getGameFactory()
                        .getCurrentGamePhase()
                        .getConfig()
                        .getString( "game.messages.boss.ownBoss" ) );

                if ( event.getDamager() instanceof Arrow ) {

                    Arrow arrow = ( ( Arrow ) event.getDamager() );
                    arrow.remove();
                }

                return;
            }
        }

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols( Locale.GERMAN );
        otherSymbols.setDecimalSeparator( '.' );
        DecimalFormat decimalFormat = new DecimalFormat( "#.##", otherSymbols );
        getZombie().setCustomName( ChatColor.translateAlternateColorCodes( '&', getName() ) + " §c" + decimalFormat.format( ( getZombie()
                .getHealth() + getZombie().getAbsorptionHearts() ) ) + " ❤" );
    }

    @EventHandler( priority = EventPriority.HIGHEST )
    public void onDamage( EntityDamageEvent event ) {

        if ( event.getEntity() != getZombie().getBukkitEntity() ) {

            return;
        }

        if ( event.getCause() == EntityDamageEvent.DamageCause.FIRE || event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK ) {
            event.getEntity().setFireTicks( 0 );
            event.setCancelled( true );
        }
    }

    @EventHandler
    public void onPlayerItemDamage( PlayerItemDamageEvent event ) {

        if ( event.getItem().getType().equals( Material.LEATHER_HELMET ) ) {

            event.setCancelled( true );
        }
    }
}
