package de.bloodgames.fighttheboss.gamemanagement.boss;

import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_12_R1.World;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;

public class CustomZombie extends EntityZombie {

    public CustomZombie( World world ) {

        super( world );

        removeGoals();
    }

    private void removeGoals( ) {

        LinkedHashSet goalB = ( LinkedHashSet ) getPrivateField( "b", PathfinderGoalSelector.class, goalSelector );
        goalB.clear();
        LinkedHashSet goalC = ( LinkedHashSet ) getPrivateField( "c", PathfinderGoalSelector.class, goalSelector );
        goalC.clear();
        LinkedHashSet targetB = ( LinkedHashSet ) getPrivateField( "b", PathfinderGoalSelector.class, targetSelector );
        targetB.clear();
        LinkedHashSet targetC = ( LinkedHashSet ) getPrivateField( "c", PathfinderGoalSelector.class, targetSelector );
        targetC.clear();
    }

    private Object getPrivateField( String fieldName, Class < PathfinderGoalSelector > clazz, Object object ) {

        Field field;
        Object object1 = null;

        try {
            field = clazz.getDeclaredField( fieldName );

            field.setAccessible( true );

            object1 = field.get( object );
        } catch ( Exception e ) {
            e.printStackTrace();
        }

        return object1;
    }
}
