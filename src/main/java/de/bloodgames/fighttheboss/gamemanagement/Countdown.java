package de.bloodgames.fighttheboss.gamemanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by André on 29.11.2017.
 */
public abstract class Countdown {

    @Getter
    @Setter
    private int startValue, endValue, currentValue;
    private BukkitRunnable runnable;
    @Getter
    private Map < Integer, List < CountdownEvent > > eventMap = new HashMap <>();
    @Setter
    private GamePhase gamePhase;
    @Getter
    @Setter
    private boolean running = false;

    public Countdown( int startValue, int endValue, GamePhase gamePhase ) {

        this.startValue = startValue;
        this.endValue = endValue;
        this.currentValue = startValue;
        this.gamePhase = gamePhase;
    }

    public void startCountdown( ) {

        if ( this.running == true ) return;
        if ( this.startValue == 0 ) return;
        if ( this.gamePhase == null ) return;

        this.runnable = new BukkitRunnable() {

            @Override
            public void run( ) {

                if ( getCurrentValue() <= getEndValue() ) {

                    onFinish();
                    if ( getCurrentValue() > getEndValue() ) return;

                    stop();
                }

                for ( Integer integer : getEventMap().keySet() ) {

                    if ( getCurrentValue() != integer ) {

                        continue;
                    }

                    getEventMap().get( integer ).forEach( ( event )->event.run() );
                }

                currentValue--;
            }
        };

        this.runnable.runTaskTimerAsynchronously( FightTheBoss.getInstance(), 0L, 20L );

        this.running = true;

        MessageHandler.debug( "GamePhase (<phase>) -> Countdown start!", "phase", this.gamePhase
                .getId() );
    }

    public void stop( ) {

        MessageHandler.debug( "GamePhase (<phase>) -> Countdown stop!", "phase", this.gamePhase
                .getId() );

        if ( this.runnable == null ) return;
        this.runnable.cancel();
        this.runnable = null;
        this.running = false;
        reset();
    }

    public void reset( ) {

        setCurrentValue( startValue );
        MessageHandler.debug( "GamePhase (<phase>) -> Countdown reset!", "phase", this.gamePhase.getId() );
    }

    public abstract void onFinish( );

    public void addEvent( CountdownEvent event, int... values ) {

        for ( Integer value : values )
            addEvent( value, event );
    }

    public void addEventRange( int from, int to, CountdownEvent event ) {

        for ( int i = Math.min( from, to ); i <= Math.max( from, to ); i++ )
            addEvent( i, event );
    }

    private void addEvent( int value, CountdownEvent event ) {

        List < CountdownEvent > cdEvents = this.eventMap.containsKey( value ) ? this.eventMap.get( value ) : new ArrayList <>();
        cdEvents.add( event );
        this.eventMap.put( value, cdEvents );
    }

    public void clearEvents( ) {

        this.eventMap.clear();
    }

    public interface CountdownEvent {

        void run( );
    }
}

