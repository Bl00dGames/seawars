package de.bloodgames.fighttheboss.commands;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.usermanagement.Stats;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStats implements CommandExecutor {

    @Override public boolean onCommand( CommandSender commandSender, Command command, String label, String[] arguments ) {

        if ( !( commandSender instanceof Player ) ) {

            System.out.println( "Nur ein Spieler kann diesen Befehl ausführen!" );
            return true;
        }

        User user = FightTheBoss.getInstance().getUserFactory().getUser( ( ( Player ) commandSender ) );

        if ( arguments.length == 0 ) {

            Stats stats = user.getStats();
            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                            .getGameFactory()
                            .getConfig()
                            .getString( "game.messages.stats" ), "user", user.getUsername(),
                    "kills", stats.getStatAsString( Stats.StatsTypes.KILLS ),
                    "deaths", stats.getStatAsString( Stats.StatsTypes.DEATHS ),
                    "bossKills", stats.getStatAsString( Stats.StatsTypes.BOSSKILLS ),
                    "wins", stats.getStatAsString( Stats.StatsTypes.WINS ),
                    "loses", stats.getStatAsString( Stats.StatsTypes.LOSES ),
                    "gamesPlayed", stats.getStatAsString( Stats.StatsTypes.GAMESPLAYED ),
                    "points", stats.getStatAsString( Stats.StatsTypes.POINTS ),
                    "kd", stats.getStatAsString( Stats.StatsTypes.KD ) );

            return true;
        }

        User user1 = new User( arguments[ 0 ] );
        Stats stats = user1.getStats();
        user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                        .getGameFactory()
                        .getConfig()
                        .getString( "game.messages.stats" ), "user", user1.getUsername(),
                "kills", stats.getStatAsString( Stats.StatsTypes.KILLS ),
                "deaths", stats.getStatAsString( Stats.StatsTypes.DEATHS ),
                "bossKills", stats.getStatAsString( Stats.StatsTypes.BOSSKILLS ),
                "wins", stats.getStatAsString( Stats.StatsTypes.WINS ),
                "loses", stats.getStatAsString( Stats.StatsTypes.LOSES ),
                "gamesPlayed", stats.getStatAsString( Stats.StatsTypes.GAMESPLAYED ),
                "points", stats.getStatAsString( Stats.StatsTypes.POINTS ),
                "kd", stats.getStatAsString( Stats.StatsTypes.KD ) );

        return true;
    }
}
