package de.bloodgames.fighttheboss.commands;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFTB implements CommandExecutor {

    @Override
    public boolean onCommand( CommandSender commandSender, Command command, String label, String[] arguments ) {

        if ( !( commandSender instanceof Player ) ) {

            System.out.println( "Nur ein Spieler kann diesen Befehl ausführen!" );
            return true;
        }

        User user = FightTheBoss.getInstance().getUserFactory().getUser( ( Player ) commandSender );

        if ( !user.getPlayer().hasPermission( "ftb.admin" ) ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance().getGameFactory().getConfig().getString( "game.messages.commands.noPermission" ) );
            return true;
        }

        if ( arguments.length == 0 ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.help" ));

            return true;
        }

        if ( arguments.length == 1 ) {

            if ( arguments[ 0 ].equalsIgnoreCase( "next" ) ) {

                user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                        .getGameFactory()
                        .getConfig()
                        .getString( "game.messages.gamePhase.skip" ), "phase", FightTheBoss.getInstance()
                        .getGameFactory()
                        .getCurrentGamePhase()
                        .getId() );
                FightTheBoss.getInstance().getGameFactory().nextPhase();
                return true;
            }
        }

        if ( arguments.length != 3 ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.help" ));
            return true;
        }

        if ( !arguments[ 0 ].equalsIgnoreCase( "setSpawn" ) ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.help" ));
            return true;
        }

        String gamePhase = arguments[ 1 ];
        String id = arguments[ 2 ];
        GamePhase gamePhase1 = null;

        for ( GamePhase phase : FightTheBoss.getInstance().getGameFactory().getGamePhaseList() ) {

            if ( phase.getId().equalsIgnoreCase( gamePhase ) ) {

                gamePhase1 = phase;
            }
        }

        if ( gamePhase1 == null ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.spawn.error" ), "game", FightTheBoss.getInstance()
                    .getGameFactory()
                    .getPrefix(), "phase", gamePhase, "spawn", id );
            return true;
        }

        gamePhase1.getSpawnFactory().addSpawn( user, id );

        return true;
    }
}
