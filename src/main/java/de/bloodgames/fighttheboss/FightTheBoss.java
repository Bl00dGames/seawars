package de.bloodgames.fighttheboss;

import de.bloodgames.fighttheboss.commands.CommandFTB;
import de.bloodgames.fighttheboss.commands.CommandStats;
import de.bloodgames.fighttheboss.gamemanagement.GameFactory;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.gamemanagement.gamephases.*;
import de.bloodgames.fighttheboss.listener.MiscListener;
import de.bloodgames.fighttheboss.teammanagement.TeamHandler;
import de.bloodgames.fighttheboss.usermanagement.UserFactory;
import de.bloodgames.fighttheboss.utils.Config;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.maps.MapUtil;
import de.bloodgames.fighttheboss.utils.sql.MySQL;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by André on 29.11.2017.
 */
public class FightTheBoss extends JavaPlugin {

    @Getter
    private static FightTheBoss instance;
    @Getter
    public final MessageHandler messageHandler = new MessageHandler();
    @Getter
    public GamePhase lobbyPhase, startPhase, inGamePhase, deathMatchPhase, endPhase;
    @Getter
    public MySQL mySQL;
    @Getter
    private PluginManager pluginManager = getServer().getPluginManager();
    @Getter
    private GameFactory gameFactory;
    @Getter
    private UserFactory userFactory = new UserFactory();
    @Getter
    private TeamHandler teamHandler;
    @Getter private Config config;

    @Override
    public void onEnable( ) {

        instance = this;

        initGame();

        pluginManager.registerEvents( new MiscListener(), this );
        getCommand( "fighttheboss" ).setExecutor( new CommandFTB() );
        getCommand( "stats" ).setExecutor( new CommandStats() );

        MessageHandler.debug( "Game initialized." );
    }

    private void initGame( ) {

        gameFactory = new GameFactory();
        config = getGameFactory().getConfig();

        teamHandler = new TeamHandler();

        lobbyPhase = new LobbyPhase();
        startPhase = new StartPhase();
        inGamePhase = new InGamePhase();
        deathMatchPhase = new DeathMatchPhase();
        endPhase = new EndPhase();

        //Init gamePhases
        getLobbyPhase().init();
        getStartPhase().init();
        getInGamePhase().init();
        getDeathMatchPhase().init();
        getEndPhase().init();

        //Init MySQL
        mySQL = new MySQL( getConfig().getString( "mysql.username" ), config.getString( "mysql.password" ), config.getString( "mysql.database" ), config.getString( "mysql.host" ), config.getInt( "mysql.port" ) );

        //Start game
        getGameFactory().startGame( getLobbyPhase() );

        Bukkit.getWorlds().forEach( world->{

            if ( !world.getName().contains( "_nether" ) && !world.getName().contains( "_the_end" ) ) {

                MapUtil.copyMap( world );
            }
        } );
    }

    @Override
    public void onDisable( ) {

        //Closing MySQL-Connection
        if ( getMySQL().getConnection() != null ) getMySQL().close();

        Bukkit.getOnlinePlayers().forEach( player->getUserFactory().unregisterUser( getUserFactory().getUser( player ) ) );

        Bukkit.getWorlds().forEach( world->{

            world.getEntities().forEach( entity->entity.remove() );
            if ( !world.getName().contains( "_nether" ) && !world.getName().contains( "_the_end" ) ) {

                MapUtil.restoreMap( world );
            }
        } );

        //Shutting down server
        MessageHandler.debug( "Game is shutting down..." );
        Bukkit.getWorlds().forEach( world->world.setAutoSave( false ) );
        Bukkit.shutdown();
    }

    /**
     * TODO
     * 1. DeathMatch
     */
}
