package de.bloodgames.fighttheboss.listener;

import de.bloodgames.fighttheboss.gamemanagement.gamephases.InGamePhase;
import de.bloodgames.fighttheboss.utils.items.ItemBuilder;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InGamePhaseListener implements Listener {

    @Getter
    private List < ItemStack > itemStackList = new ArrayList <>();

    @Getter
    private List < Material > materialList = new ArrayList <>();

    @Getter
    private final InGamePhase inGamePhase;

    public InGamePhaseListener( InGamePhase inGamePhase ) {

        this.inGamePhase = inGamePhase;

        itemStackList.add( new ItemBuilder( Material.WOOD_SWORD ).build() );
        itemStackList.add( new ItemBuilder( Material.BOW ).addEnchantment( Enchantment.ARROW_INFINITE, 1 ).build() );
        itemStackList.add( new ItemBuilder( Material.STONE_SWORD ).build() );
        itemStackList.add( new ItemBuilder( Material.COOKED_BEEF ).setAmount( 16 ).build() );
        itemStackList.add( new ItemBuilder( Material.TNT ).setName( "&fDynamit" ).build() );

        getItemStackList().forEach( itemStack1->materialList.add( itemStack1.getType() ) );
    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {

        if ( event.getClickedBlock() == null || event.getClickedBlock().getType().equals( Material.AIR ) ) {

            return;
        }

        if ( !event.getClickedBlock().getType().equals( Material.CHEST ) ) {

            return;
        }

        if ( event.getAction() != Action.RIGHT_CLICK_BLOCK ) {

            return;
        }

        event.setCancelled( true );
        Block block = event.getClickedBlock();
        Player player = event.getPlayer();

        if ( getInGamePhase().getUserFactory().getUser( player ).isSpectator() ) {
            return;
        }

        boolean hasAllItems = true;

        List < Material > materialListPlayerInventory = new ArrayList <>();

        if ( player.getInventory().getItem( 0 ) != null ) {

            for ( Material material : materialList ) {

                for ( ItemStack itemStack : player.getInventory().getContents() ) {

                    if ( itemStack != null && itemStack.getType() != Material.AIR ) {

                        materialListPlayerInventory.add( itemStack.getType() );
                    }
                }

                if ( !materialListPlayerInventory.contains( material ) ) {

                    hasAllItems = false;
                }
            }
        } else {

            hasAllItems = false;
        }

        if ( hasAllItems ) {
            return;
        }


        if ( !inGamePhase.getChestLocations().contains( block.getLocation() ) ) {

            inGamePhase.getChestLocations().add( block.getLocation() );
        }

        block.setType( Material.AIR );

        Random random = new Random();
        ItemStack chosenItem = itemStackList.get( random.nextInt( itemStackList.size() ) );
        while ( materialListPlayerInventory.contains( chosenItem.getType() ) ) {
            chosenItem = itemStackList.get( random.nextInt( itemStackList.size() ) );
        }

        player.getInventory().addItem( chosenItem );
        if ( chosenItem.getType() == Material.BOW ) {
            player.getInventory().addItem( new ItemBuilder( Material.ARROW ).build() );
        }

        player.updateInventory();
    }

    @EventHandler( priority = EventPriority.HIGHEST )
    public void BlockPlaceEvent( BlockPlaceEvent event ) {

        if ( event.getBlockPlaced().getType() == Material.TNT ) {

            event.getBlockPlaced()
                    .getLocation()
                    .getWorld()
                    .spawnEntity( event.getBlockPlaced().getLocation(), EntityType.PRIMED_TNT );
            event.setCancelled( true );

            Player player = event.getPlayer();
            ItemBuilder itemBuilder = new ItemBuilder().fromItemStack( event.getItemInHand() );
            int amount = itemBuilder.getItemStack().clone().getAmount() - 1;
            itemBuilder.setAmount( amount );
            player.getInventory()
                    .setItemInMainHand( ( amount == 0 ? new ItemStack( Material.AIR ) : itemBuilder.build() ) );
            return;
        }
    }

}
