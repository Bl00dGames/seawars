package de.bloodgames.fighttheboss.listener;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.teammanagement.Team;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Created by André on 01.12.2017.
 */
public class MiscListener implements Listener {

    @EventHandler
    public void onBlockBreak( BlockBreakEvent event ) {

        if ( event.getPlayer().getGameMode() == GameMode.CREATIVE ) {
            return;
        }
        event.setCancelled( true );
    }

    @EventHandler
    public void onBlockPlace( BlockPlaceEvent event ) {

        if ( event.getPlayer().getGameMode() == GameMode.CREATIVE ) {
            return;
        }
        event.setCancelled( true );
    }

    @EventHandler
    private void onBlockDamage( BlockDamageEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    private void onBlockExplode( BlockExplodeEvent event ) {

        event.blockList().clear();
        event.setCancelled( true );
    }

    @EventHandler
    private void onEntityExplode( EntityExplodeEvent event ) {

        event.blockList().clear();
        event.setCancelled( true );
    }

    @EventHandler
    public void onEntityDamageByEntity( EntityDamageByEntityEvent event ) {

        if ( FightTheBoss.getInstance().getGameFactory().isPvp() ) {

            event.setCancelled( true );
        }

        if ( event.getEntity() instanceof Player ) {
            if ( FightTheBoss.getInstance().getUserFactory().getUser( ( Player ) event.getEntity() ).isSpectator() ) {

                event.setCancelled( true );
            }
        }

        if ( event.getDamager() != null ) {
            if ( event.getDamager() instanceof Player ) {
                if ( FightTheBoss.getInstance()
                        .getUserFactory()
                        .getUser( ( Player ) event.getDamager() )
                        .isSpectator() ) {

                    event.setCancelled( true );
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamage( EntityDamageEvent event ) {

        if ( FightTheBoss.getInstance().getGameFactory().getCurrentGamePhase() == FightTheBoss.getInstance()
                .getEndPhase() || FightTheBoss.getInstance()
                .getGameFactory()
                .getCurrentGamePhase() == FightTheBoss.getInstance().getLobbyPhase() ) {

            event.setCancelled( true );
        }
    }

    @EventHandler
    public void onMove( PlayerMoveEvent event ) {

        event.setCancelled( false );

        if ( FightTheBoss.getInstance().getGameFactory().isAllowedToMove() || FightTheBoss.getInstance()
                .getUserFactory()
                .getUser( event.getPlayer() )
                .isSpectator() ) {

            return;
        }

        if ( event.getFrom().getBlockX() == event.getTo().getBlockX() && event.getFrom().getBlockZ() == event.getTo()
                .getBlockZ() ) {
            return;
        }

        event.setCancelled( true );
    }

    @EventHandler
    public void onFoodLevel( FoodLevelChangeEvent event ) {

        if ( FightTheBoss.getInstance().getGameFactory().getCurrentGamePhase() == FightTheBoss.getInstance()
                .getInGamePhase() || FightTheBoss.getInstance()
                .getUserFactory()
                .getUser( ( Player ) event.getEntity() )
                .isSpectator() ) {
            return;
        }

        event.setCancelled( true );
    }

    @EventHandler
    public void onServerListPing( ServerListPingEvent event ) {

        event.setMotd( "Current GamePhase -> " + FightTheBoss.getInstance()
                .getGameFactory()
                .getCurrentGamePhase()
                .getId() );
    }

    @EventHandler
    public void onEntitySpawn( CreatureSpawnEvent event ) {

        if ( event.getEntityType() == EntityType.PRIMED_TNT ) {

            return;
        }

        if ( FightTheBoss.getInstance().getGameFactory().getCurrentGamePhase() != FightTheBoss.getInstance()
                .getStartPhase() ) {

            event.setCancelled( true );
            return;
        }

        boolean allowedToSpawn = false;

        for ( Team team : FightTheBoss.getInstance().getTeamHandler().getTeamList() ) {

            if ( team == null || team.getBoss() == null || team.getBoss().getZombie() == null || team.getBoss()
                    .getZombie()
                    .getBukkitEntity() == null ) {

                allowedToSpawn = false;
                continue;
            }

            if ( event.getEntity().getCustomName() == null ) {

                allowedToSpawn = false;
                continue;
            }

            if ( event.getEntity().getType() != team.getBoss().getZombie().getBukkitEntity().getType() ) {

                allowedToSpawn = false;
                continue;
            }

            allowedToSpawn = true;
            break;
        }


        event.setCancelled( !allowedToSpawn );
    }

    @EventHandler
    public void onDeath( EntityDeathEvent event ) {

        event.getDrops().clear();
    }

    @EventHandler
    public void onWeatherChange( WeatherChangeEvent event ) {

        if ( event.toWeatherState() ) {

            event.setCancelled( true );
        }
    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {

        if ( event.getClickedBlock() == null || event.getClickedBlock().getType().equals( Material.AIR ) ) {

            return;
        }

        if ( !event.getClickedBlock().getType().equals( Material.CHEST ) ) {

            return;
        }

        event.setCancelled( true );
    }

    @EventHandler
    public void onDrop( PlayerDropItemEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    public void onPickUp( EntityPickupItemEvent event ) {

        event.setCancelled( true );
    }
}
