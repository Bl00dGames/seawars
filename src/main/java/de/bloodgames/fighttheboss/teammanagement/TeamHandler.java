package de.bloodgames.fighttheboss.teammanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.GameFactory;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import org.bukkit.Color;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by André on 30.11.2017.
 */
public class TeamHandler {

    @Getter
    private List < Team > teamList = new LinkedList <>();

    public TeamHandler( ) {

        init();
    }

    public Team getTeamById( String id ) {

        for ( Team team : teamList ) {

            if ( team.getId().equalsIgnoreCase( id ) ) {

                return team;
            }
        }

        return null;
    }

    public Team getTeamByUser( User user ) {

        for ( Team team : getTeamList() ) {

            if ( team.getUserList().contains( user ) ) {

                return team;
            }
        }

        return null;
    }

    private void init( ) {

        Team team1 = new Team( "team1", "&9Blau", FightTheBoss.getInstance()
                .getGameFactory()
                .getTeamSize(), Color.BLUE );
        Team team2 = new Team( "team2", "&cRot", FightTheBoss.getInstance().getGameFactory().getTeamSize(), Color.RED );

        teamList.add( team1 );
        teamList.add( team2 );
    }

    public TeamHandler autoSelect( ) {

        /**TODO auto team selection
         * Balance teams*/

        List < User > noTeam = new ArrayList <>();

        GameFactory gameFactory = FightTheBoss.getInstance().getGameFactory();
        gameFactory.getInGameUsers().forEach( user->{

            if ( user.getTeam() == null ) {

                noTeam.add( user );
            }
        } );

        if ( noTeam.size() != 0 ) {
            Iterator < User > users = noTeam.iterator();

            while ( users.hasNext() ) {
                User user = users.next();
                selectTeam( user, getPrioritizedTeam() );
                users.remove();
            }
        }


        Team team1 = getTeamList().get( 0 );
        Team team2 = getTeamList().get( 1 );

        int amountOfTeam1 = team1.getTeamSize();
        int amountOfTeam2 = team2.getTeamSize();
        int amountOfIngamePlayers = gameFactory.getInGameUsers().size();

        if ( amountOfTeam1 + amountOfTeam2 == amountOfIngamePlayers ) {

            if ( amountOfIngamePlayers % 2 == 0 ) {

                while ( getDifference( team1.getTeamSize(), team2.getTeamSize() ) >= 1 ) {

                    balanceTeam( team2 );
                }

                while ( getDifference( team1.getTeamSize(), team2.getTeamSize() ) <= -1 ) {

                    balanceTeam( team1 );
                }

                return this;
            }

            while ( getDifference( team1.getTeamSize(), team2.getTeamSize() ) > 1 ) {

                balanceTeam( team2 );
            }

            while ( getDifference( team1.getTeamSize(), team2.getTeamSize() ) < -1 ) {

                balanceTeam( team1 );
            }

            return this;
        }


        return this;
    }

    private Team getPrioritizedTeam( ) {

        Team team1 = getTeamList().get( 0 );
        Team team2 = getTeamList().get( 1 );

        if ( team1.getTeamSize() == team2.getTeamSize() ) return team1;
        if ( team1.getTeamSize() > team2.getTeamSize() ) return team2;
        if ( team1.getTeamSize() < team2.getTeamSize() ) return team1;

        return team1;
    }

    private int getDifference( int value1, int value2 ) {

        return value1 - value2;
    }

    private TeamHandler balanceTeam( Team targetTeam ) {

        Team team1 = getTeamList().get( 0 );
        Team team2 = getTeamList().get( 1 );

        if ( targetTeam == team1 ) {

            User user = team2.getRandomUser();
            selectTeam( user, team1 );

            return this;
        }

        User user = team1.getRandomUser();
        selectTeam( user, team2 );

        return this;
    }

    public TeamHandler unselectTeam( User user ) {

        if ( user.getTeam() == null ) {

            return this;
        }

        user.getTeam().removePlayer( user );
        user.setTeam( null );

        return this;
    }

    public TeamHandler selectTeam( User user, Team team ) {

        if ( team.getTeamSize() >= team.getDefaultTeamSize() ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.teamHandler.team.isFull" ), "game", FightTheBoss.getInstance()
                    .getGameFactory()
                    .getPrefix(), "team", team.getDisplayName() );
            return this;
        }

        unselectTeam( user );

        team.addPlayer( user );
        user.setTeam( team );

        return this;
    }
}
