package de.bloodgames.fighttheboss.teammanagement;

import de.bloodgames.fighttheboss.gamemanagement.boss.Boss;
import de.bloodgames.fighttheboss.spawnmanagement.Spawn;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.items.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Zombie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by André on 30.11.2017.
 */
public class Team {

    @Getter
    private final String id, displayName;

    @Getter
    private final int defaultTeamSize;

    @Getter
    private List < User > userList = new ArrayList <>(), deadUserList = new ArrayList <>();
    @Getter
    @Setter
    private boolean respawnable = true;

    @Getter @Setter
    private Color color;

    @Getter @Setter
    private Spawn bossSpawn;

    @Getter @Setter
    private Boss boss;

    public Team( String id, String displayName, int defaultTeamSize, Color color ) {

        this.id = id;
        this.displayName = displayName;
        this.defaultTeamSize = defaultTeamSize;
        this.color = color;
        clearPlayers();

        MessageHandler.debug( "Team <team> is initialized.", "team", getId() );
    }

    public Team spawnBoss( Spawn spawn ) {

        if ( spawn == null ) {

            MessageHandler.debug( "Team <team>: Boss is not set yet!", "team", getId() );
            return this;
        }

        boss = new Boss( displayName, spawn, this );
        boss.setAllowedMovement( false );
        boss.onInitSpawnEntity();

        Zombie zombie = ( ( Zombie ) boss.getZombie().getBukkitEntity() );
        zombie.getEquipment()
                .setHelmet( new ItemBuilder( Material.LEATHER_HELMET ).setColor( color )
                        .addEnchantment( Enchantment.DURABILITY, 3 )
                        .build() );

        return this;
    }

    public Team addPlayer( User user ) {

        userList.add( user );

        MessageHandler
                .debug( "User (<user>) added to <team>.", "user", user.getUsername(), "team", getId() );

        return this;
    }

    public Team removePlayer( User user ) {

        if ( userList.isEmpty() ) return this;
        if ( !userList.contains( user ) ) return this;

        userList.remove( user );

        MessageHandler
                .debug( "User (<user>) removed of <team>.", "user", user.getUsername(), "team", getId() );

        return this;
    }

    public Team clearPlayers( ) {

        userList.clear();
        MessageHandler.debug( "Team <team> was cleared.", "team", getId() );

        return this;
    }

    public boolean isEmpty( ) {

        return userList.isEmpty();
    }

    public String getColorCode( ) {

        return displayName.substring( 0, displayName.length() - ( displayName.length() - 2 ) );
    }

    public int getTeamSize( ) {

        return userList.size();
    }

    public User getRandomUser( ) {

        Random random = new Random();

        System.out.println( userList.size() );

        User user = userList.get( random.nextInt( userList.size() ) );
        while ( user == null ) {

            user = userList.get( random.nextInt( userList.size() ) );
        }

        return user;
    }
}
