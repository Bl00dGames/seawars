package de.bloodgames.fighttheboss.usermanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

public class Scoreboard {

    @Getter
    private final String title;

    @Getter
    private final User user;

    @Getter
    private Map<Integer, Map<String, String>> stringMap = new HashMap<>(  );

    @Getter
    private org.bukkit.scoreboard.Scoreboard scoreboard;

    @Getter
    private BukkitTask bukkitTask;
    @Getter @Setter
    private Objective objective;

    public Scoreboard( String title, User user ) {

        this.title = title;
        this.user = user;
    }

    public Scoreboard addLine(String teamId, String line){

        int id = 20-getStringMap().size();
        if(id < 0){
            id = 0;
        }

        Map<String, String> stringStringMap = new HashMap <>(  );
        stringStringMap.put( teamId, line );

        getStringMap().put( id, stringStringMap );

        return this;
    }

    public Scoreboard addLine(String line){

        addLine( "none", line );

        return this;
    }

    public Scoreboard setScoreboard(){

        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Player player = user.getPlayer();
        scoreboard.clearSlot( DisplaySlot.SIDEBAR );

        setObjective( scoreboard.registerNewObjective( user.getUsername() , "dummy" ) );
        getObjective().setDisplayName( title );
        getObjective().setDisplaySlot( DisplaySlot.SIDEBAR );

        stringMap.forEach( ( integer, stringMap ) -> {

            ChatColor chatColor = ChatColor.values()[new Random(  ).nextInt( ChatColor.values().length-1 )];

            for ( Team team : getScoreboard().getTeams() ) {

                while (  team.hasEntry( chatColor + "" ) ) {

                    chatColor = ChatColor.values()[new Random(  ).nextInt( ChatColor.values().length-1 )];
                }
            }

            while ( chatColor.isFormat() ) {
                chatColor = ChatColor.values()[new Random(  ).nextInt( ChatColor.values().length-1 )];
            }

            if ( !stringMap.keySet().contains( "none" ) ) {
                for ( Map.Entry < String, String > stringEntry : stringMap.entrySet() ) {

                    Team team = getScoreboard().registerNewTeam( stringEntry.getKey() );
                    team.addEntry( chatColor + "" );
                    team.setPrefix( formatLine( stringEntry.getValue() ) );

                    getObjective().getScore( chatColor + "").setScore( integer );
                }

                return;
            }
            for ( Map.Entry < String, String > stringEntry : stringMap.entrySet() ) {

                getObjective().getScore( stringEntry.getValue() ).setScore( integer );
            }

        } );


        bukkitTask = Bukkit.getScheduler().runTaskTimer( FightTheBoss.getInstance(), new Runnable() {
            @Override public void run( ) {

                updateScoreboard();
            }
        }, 0L, 20L );

        player.setScoreboard( scoreboard );

        return this;
    }

    public Scoreboard unregisterScoreboard(){

        user.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        return this;
    }

    public Scoreboard updateScoreboard(){

        stringMap.forEach( ( integer, stringMap ) -> {

            if ( !stringMap.keySet().contains( "none" ) ) {
                for ( Map.Entry < String, String > stringEntry : stringMap.entrySet() ) {

                    Team team = getScoreboard().getTeam( stringEntry.getKey() );
                    team.setPrefix( formatLine( stringEntry.getValue() ) );
                }
            }
        });

        return this;
    }

    private String formatLine(String line){

        Map<String, String> stringStringMap = new HashMap <>(  );
        stringStringMap.put( "team", user.getTeam().getDisplayName() );
        stringStringMap.put( "map", Bukkit.getWorlds().get( 0 ).getName() );
        stringStringMap.put( "kills", String.valueOf( user.getStats().getStat( Stats.StatsTypes.KILLS ) - user.getStats().getCachedStatsTypesIntegerMap().get( Stats.StatsTypes.KILLS ) ) );
        stringStringMap.put( "players", String.valueOf( FightTheBoss.getInstance().getGameFactory().getIngameUserAmount() ) );
        double currentTime = FightTheBoss.getInstance().getGameFactory().getCurrentGamePhase().getCountdown().getCurrentValue();
        String[] timeSections = String.valueOf( currentTime/60 ).split( Pattern.quote( "." ) );
        String seconds = String.valueOf((currentTime-(Integer.valueOf( timeSections[0] )*60))).split( Pattern.quote( "." ) )[0];
        stringStringMap.put( "time", timeSections[0] + ":" + seconds);

        for ( Map.Entry < String, String > entry : stringStringMap.entrySet() ) {
            line = line.replaceAll( "\\<" + entry.getKey() + "\\>", entry.getValue() );
        }

        return ChatColor.translateAlternateColorCodes( '&', line );
    }
}
