package de.bloodgames.fighttheboss.usermanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.spawnmanagement.Spawn;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.inventory.InventoryGui;
import de.bloodgames.fighttheboss.utils.items.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by André on 29.11.2017.
 */
public class User {

    @Getter
    private final String username;
    @Getter
    private final UUID uuid;
    @Getter
    private final Player player;
    @Getter
    @Setter
    private Team team;

    @Getter
    private final Stats stats;

    @Getter
    @Setter
    private boolean spectator = false;

    @Getter @Setter
    private Scoreboard scoreboard;

    @Getter
    private List < InventoryGui > inventoryGuiList = new ArrayList <>();

    public User( Player player ) {

        username = player.getName();
        uuid = player.getUniqueId();
        this.player = player;
        this.stats = new Stats( this );
    }

    public User( String username ) {

        this.username = username;
        this.uuid = null;
        this.player = null;
        this.stats = new Stats( this );
    }

    public void sendMessage( String message ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        getPlayer().sendMessage( message );
    }

    public void sendMessage( String message, String... variables ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        getPlayer().sendMessage( MessageHandler.getMessage( message, variables ) );
    }

    public void sendMessage( MessageHandler.MessageType messageType, String message, String... variables ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        getPlayer().sendMessage( MessageHandler.getMessage( messageType, message, variables ) );
    }

    public void teleport( Spawn spawn ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        spawn.teleportUser( this );
    }

    public void sendToLobby( ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        if ( !getPlayer().getListeningPluginChannels().contains( "BungeeCord" ) ) {
            return;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream( byteArrayOutputStream );


        try {
            dataOutputStream.writeUTF( "Connect" );
            dataOutputStream.writeUTF( "lobby" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        getPlayer().sendPluginMessage( FightTheBoss.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray() );

    }

    public void setSpectator( boolean spectator ) {

        if ( getPlayer() == null ) {

            MessageHandler.debug( "User (<user>) not online!", "user", getUsername() );
            return;
        }

        this.spectator = spectator;
        getPlayer().setGameMode( GameMode.SURVIVAL );

        if ( !spectator ) {

            getPlayer().setAllowFlight( false );
            getPlayer().setFlying( false );
            Bukkit.getOnlinePlayers().forEach( player1 -> player1.showPlayer(FightTheBoss.getInstance(), this.getPlayer() ) );
            return;
        }

        getPlayer().setAllowFlight( true );
        getPlayer().setFlying( true );
        getPlayer().setCollidable( false );

        FightTheBoss.getInstance().getGameFactory().getCurrentGamePhase().getSpawnFactory().getSpawnById( "spectator.spawn" ).teleportUser( getPlayer() );
        FightTheBoss.getInstance().getGameFactory().getSpectators().add( this );

        FightTheBoss.getInstance()
                .getGameFactory()
                .getInGameUsers()
                .forEach( user->user.getPlayer().hidePlayer( FightTheBoss.getInstance(), this.getPlayer() ) );

        if ( getTeam() != null ) {

            getTeam().getUserList().remove( this );
            getTeam().getDeadUserList().add( this );
        }

        ItemStack itemStack = new ItemBuilder( Material.COMPASS ).setName( "&7Spectator-Menu" )
                .addLore( "&7Teleportiere dich zu einem Spieler." )
                .build();
        getPlayer().getInventory().setItem( 0, itemStack );
        getPlayer().updateInventory();

        new InventoryGui( "&7Spectator-Menu", 2 * 9, itemStack, getPlayer() ) {

            @Override public void addItems( ) {

                for ( User user : FightTheBoss.getInstance().getGameFactory().getInGameUsers() ) {

                    getInventory().addItem( new ItemBuilder( Material.SKULL_ITEM ).setName( user.getUsername() )
                            .addLore( "&7Teleportiere dich zu &7" + user.getUsername() )
                            .setSkullOwner( user.getUsername() )
                            .build() );
                }
            }

            @Override public void onClick( Player player, InventoryClickEvent clickEvent ) {

                if ( clickEvent.getCurrentItem() == null || clickEvent.getCurrentItem().getType() == Material.AIR ) {

                    return;
                }

                Player targetPlayer = Bukkit.getPlayer( ChatColor.stripColor( clickEvent.getCurrentItem()
                        .getItemMeta()
                        .getDisplayName() ) );
                if ( targetPlayer == null || !targetPlayer.isOnline() ) {

                    sendMessage( FightTheBoss.getInstance()
                            .getGameFactory()
                            .getConfig()
                            .getString( "game.messages.player.isOffline" ), "player", targetPlayer.getDisplayName() );
                    closeGui();
                    return;
                }

                player.teleport( targetPlayer );
            }

            @Override public void updateItems( ) {

                for ( ItemStack stack : getInventory().getContents() ) {

                    if ( itemStack == null || itemStack.getType() == Material.AIR || itemStack.getType() != Material.SKULL_ITEM ) {

                        continue;
                    }

                    User user = FightTheBoss.getInstance()
                            .getUserFactory()
                            .getUser( ChatColor.stripColor( itemStack.getItemMeta().getDisplayName() ) );
                    if ( user.isSpectator() || user.getPlayer() == null || !user.getPlayer().isOnline() ) {

                        getInventory().remove( stack );
                    }
                }
            }
        };

        MessageHandler.debug( "User (<user>) -> Now spectator!", "user", getUsername() );
    }

    public void setScoreboard(){

        setScoreboard( new Scoreboard( ChatColor.BOLD + "GOMMEHD.NET" , this) );
        getScoreboard().addLine( " " );
        getScoreboard().addLine( "§fTeam§8:" );
        getScoreboard().addLine( "team", "§f<team>" );
        getScoreboard().addLine( "  " );
        getScoreboard().addLine( "§fMap§8:" );
        getScoreboard().addLine( "map", "§e<map>" );
        getScoreboard().addLine( "   " );
        getScoreboard().addLine( "§fKills§8:" );
        getScoreboard().addLine( "kills","§c<kills>" );
        getScoreboard().addLine( "    " );
        getScoreboard().addLine( "","§fSpieler§8:" );
        getScoreboard().addLine( "player","§f<players>" );
        getScoreboard().addLine( "     " );
        getScoreboard().addLine( "§fZeit§8:" );
        getScoreboard().addLine( "time","§e<time>" );
        getScoreboard().setScoreboard( );
    }
}
