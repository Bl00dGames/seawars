package de.bloodgames.fighttheboss.usermanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import de.bloodgames.fighttheboss.utils.sql.MySQL;
import lombok.Getter;
import lombok.Setter;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Stats {

    @Getter @Setter
    private Map < StatsTypes, Long > statsTypesIntegerMap = new HashMap <>();

    @Getter @Setter
    private Map < StatsTypes, Long > cachedStatsTypesIntegerMap = new HashMap <>();

    @Getter
    private User user;

    public Stats( User user ) {

        this.user = user;
        init();
    }

    public Stats init( ) {

        MySQL mySQL = FightTheBoss.getInstance().getMySQL();

        mySQL.connectToDatabase();
        try {
            mySQL.getConnection().prepareStatement( "CREATE TABLE IF NOT EXISTS stats (" +
                    " id INT NOT NULL AUTO_INCREMENT," +

                    "username VARCHAR(32) default 'name'," +
                    "kills BIGINT default 0," +
                    "deaths BIGINT default 0," +
                    "bossKills BIGINT default 0," +
                    "wins BIGINT default 0," +
                    "loses BIGINT default 0," +
                    "gamesPlayed BIGINT default 0," +
                    "points BIGINT default 0," +
                    "PRIMARY KEY (id, username));" ).executeUpdate();
        } catch ( SQLException e ) {
            System.err.println( "SQLException: " + e.getMessage() );
            System.err.println( "SQLState: " + e.getSQLState() );
            System.err.println( "VendorError: " + e.getErrorCode() );
        }

        if ( mySQL.isInTable( "stats", "username", user.getUsername() ) ) {

            setStat( StatsTypes.KILLS, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "kills" ) ) );
            setStat( StatsTypes.DEATHS, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "deaths" ) ) );
            setStat( StatsTypes.BOSSKILLS, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "bossKills" ) ) );
            setStat( StatsTypes.WINS, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "wins" ) ) );
            setStat( StatsTypes.LOSES, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "loses" ) ) );
            setStat( StatsTypes.GAMESPLAYED, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "gamesPlayed" ) ) );
            setStat( StatsTypes.POINTS, ( ( long ) mySQL.get( "stats", "username", user.getUsername(), "points" ) ) );
            setStat( StatsTypes.KD, ( getStat( StatsTypes.DEATHS ) == getStat( StatsTypes.KILLS ) ? 0 : getStat( StatsTypes.KILLS ) / getStat( StatsTypes.DEATHS ) ) );

            MessageHandler.debug( "User (<user>) -> Stats loaded!", "user", user.getUsername() );
        } else {
            mySQL.insert( "stats", "`username`,`kills`,`deaths`,`bossKills`,`wins`,`loses`,`gamesPlayed`,`points`", "'" + user
                    .getUsername() + "','0','0','0','0','0','0','0'" );
        }

        getCachedStatsTypesIntegerMap().putAll( getStatsTypesIntegerMap() );
        MessageHandler.debug( "User (<user>) -> Stats initialized!", "user", user.getUsername() );
        return this;
    }

    public Stats setStat( StatsTypes statsTypes, long value ) {

        if ( statsTypesIntegerMap.containsKey( statsTypes ) ) {
            statsTypesIntegerMap.remove( statsTypes );
        }

        statsTypesIntegerMap.put( statsTypes, value );

        return this;
    }

    public Long getStat( StatsTypes statsTypes ) {

        if ( !statsTypesIntegerMap.containsKey( statsTypes ) ) {

            return 0L;
        }

        return statsTypesIntegerMap.get( statsTypes );
    }

    public String getStatAsString( StatsTypes statsTypes ) {


        return String.valueOf( getStat( statsTypes ) );
    }

    public Stats addStat( StatsTypes statsTypes, long valueToAdd ) {

        long finalValue = getStat( statsTypes ) + valueToAdd;

        setStat( statsTypes, finalValue );

        return this;
    }

    public Stats saveStats( ) {

        if ( statsTypesIntegerMap == cachedStatsTypesIntegerMap ) {

            MessageHandler.debug( "User (<user>) -> No changes of stats!", "user", user.getUsername() );
            return this;
        }

        MySQL mySQL = FightTheBoss.getInstance().getMySQL();

        mySQL.connectToDatabase();

        statsTypesIntegerMap.forEach( ( statsTypes, aLong )->{
            if ( cachedStatsTypesIntegerMap.get( statsTypes ) != aLong ) {
                if ( statsTypes != StatsTypes.KD ) {
                    mySQL.update( "stats", statsTypes.getId(), aLong, "username", user.getUsername(), true );
                }
            }
        } );

        MessageHandler.debug( "User (<user>) -> New stats saved!", "user", user.getUsername() );

        return this;
    }

    public enum StatsTypes {

        KILLS( "kills" ),
        DEATHS( "deaths" ),
        BOSSKILLS( "bossKills" ),
        WINS( "wins" ),
        LOSES( "loses" ),
        GAMESPLAYED( "gamesPlayed" ),
        POINTS( "points" ),
        KD( "kd" );

        @Getter
        private String id;

        StatsTypes( String id ) {

            this.id = id;
        }
    }
}
