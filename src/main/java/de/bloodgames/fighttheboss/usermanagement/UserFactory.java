package de.bloodgames.fighttheboss.usermanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by André on 29.11.2017.
 */
public class UserFactory {

    private Map < String, User > userMap = new HashMap <>();

    public UserFactory registerUser( User user ) {

        userMap.put( user.getUsername(), user );

        MessageHandler.debug( "User (<user>) registered successfully.", "user", user
                .getUsername() );

        return this;
    }

    public UserFactory unregisterUser( User user ) {

        if ( userMap.isEmpty() ) return this;
        if ( !userMap.containsValue( user ) ) return this;

        user.getStats().saveStats();

        userMap.remove( user.getUsername() );

        if ( FightTheBoss.getInstance().getGameFactory().getInGameUsers().contains( user ) ) {

            FightTheBoss.getInstance().getGameFactory().getInGameUsers().remove( user );
        }

        if ( user.getTeam() != null ) {

            user.getTeam().removePlayer( user );
        }

        MessageHandler.debug( "User (<user>) unregistered successfully.", "user", user
                .getUsername() );

        return this;
    }

    public User getUser( String username ) {

        if ( username == null || username.isEmpty() ) {

            MessageHandler.debug( "Username is null: " + username );
            return null;
        }
        if ( !userMap.containsKey( username ) ) {

            MessageHandler.debug( "Username is not in userMap: " + username );
            return null;
        }

        return userMap.get( username );
    }

    public User getUser( Player player ) {

        return getUser( player.getName() );
    }

    public int getUserAmount( ) {

        return userMap.size();
    }
}
