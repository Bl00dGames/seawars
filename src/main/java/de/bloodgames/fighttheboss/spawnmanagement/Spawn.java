package de.bloodgames.fighttheboss.spawnmanagement;

import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Spawn {

    @Getter
    private final String id;
    @Getter
    @Setter
    private Location location;
    @Getter
    @Setter
    private boolean used = false;

    public Spawn( String id ) {

        this.id = id;
    }

    public Spawn( Location location ) {

        this( location.toString() );

        this.location = location;
    }

    public Spawn teleportUser( User user ) {

        teleportUser( user.getPlayer() );

        return this;
    }

    public Spawn teleportUser( Player player ) {

        if ( location == null ) {

            MessageHandler.debug( "Spawn <spawn> is not set yet.", "spawn", getId() );
            return this;
        }

        if ( Bukkit.getServer().getWorlds().contains( location.getWorld() ) ) {

            MessageHandler.debug( "Spawn <spawn> is set yet.", "spawn", getId() );

        }

        player.teleport( location );

        MessageHandler
                .debug( "User (<user>) was teleported to Spawn <spawn>", "user", player.getName(), "spawn", getId() );

        return this;
    }
}
