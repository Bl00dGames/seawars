package de.bloodgames.fighttheboss.spawnmanagement;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.gamemanagement.GamePhase;
import de.bloodgames.fighttheboss.teammanagement.Team;
import de.bloodgames.fighttheboss.usermanagement.User;
import de.bloodgames.fighttheboss.utils.Config;
import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class SpawnFactory {

    @Getter
    private GamePhase gamePhase;

    @Getter
    private Config config;

    @Getter
    private List < Spawn > spawnList = new ArrayList <>();

    @Getter @Setter
    private Map < String, List < Spawn > > teamSpawnMap = new HashMap <>();

    public SpawnFactory( GamePhase gamePhase ) {

        this.gamePhase = gamePhase;

        config = new Config( "plugins/FightTheBoss/spawns", gamePhase.getId() + "Spawns" );
    }

    public SpawnFactory addSpawn( User user, String spawnId ) {

        Spawn spawn = new Spawn( spawnId );
        spawn.setLocation( user.getPlayer().getLocation() );

        if ( spawnList.contains( spawn ) ) spawnList.remove( spawn );
        spawnList.add( spawn );

        if ( spawnId.contains( "team." ) ) {

            saveSpawn( spawn, spawnId.split( "." )[ 1 ] );
        } else {

            saveSpawn( spawn );
        }

        user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                .getGameFactory()
                .getConfig()
                .getString( "game.messages.spawn.added" ), "game", FightTheBoss.getInstance()
                .getGameFactory()
                .getPrefix(), "phase", gamePhase.getId(), "spawn", spawnId );

        return this;
    }

    public SpawnFactory removeSpawn( User user, String spawnId ) {

        if ( config.get( "spawns." + spawnId ) == null ) {

            user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                    .getGameFactory()
                    .getConfig()
                    .getString( "game.messages.location.exists.not" ), "phase", gamePhase.getId(), "spawn", spawnId );
            return this;
        }

        if ( spawnList.contains( spawnId ) ) spawnList.remove( spawnId );
        config.set( "spawns." + spawnId, null );

        try {
            config.save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        user.sendMessage( MessageHandler.MessageType.GAME, FightTheBoss.getInstance()
                .getGameFactory()
                .getConfig()
                .getString( "game.messages.location.delete" ), "phase", gamePhase.getId(), "spawn", spawnId );

        return this;
    }

    private void saveSpawn( Spawn spawn ) {

        Location location = spawn.getLocation();

        config.set( "spawns." + spawn.getId() + ".world", location.getWorld().getName() );
        config.set( "spawns." + spawn.getId() + ".x", location.getX() );
        config.set( "spawns." + spawn.getId() + ".y", location.getY() );
        config.set( "spawns." + spawn.getId() + ".z", location.getZ() );
        config.set( "spawns." + spawn.getId() + ".yaw", location.getYaw() );
        config.set( "spawns." + spawn.getId() + ".pitch", location.getPitch() );

        try {
            config.save();
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        MessageHandler.debug( "GamePhase (<phase>) saved spawn: <spawn>", "phase", gamePhase.getId(), "spawn", spawn.getId() );
    }

    private void saveSpawn( Spawn spawn, String teamId ) {

        saveSpawn( spawn );

        addTeamSpawn( teamId, spawn );
    }

    public void loadSpawns( ) {

        if ( config.get( "spawns" ) == null ) return;

        ConfigurationSection configurationSection = getConfig().getConfigurationSection( "spawns" );
        Set < String > keys = configurationSection.getKeys( false );

        for ( String key : keys ) {

            ConfigurationSection subSection = configurationSection.getConfigurationSection( key );
            Set < String > subKeys = subSection.getKeys( false );

            for ( String subKey : subKeys ) {

                Spawn spawn = new Spawn( configurationSection.getName() + "." + subSection.getName() + "." + subKey );

                Location location = new Location( Bukkit.getWorld( config.getString( spawn.getId() + ".world" ) ), config
                        .getDouble( spawn.getId() + ".x" ), config.getDouble( spawn.getId() + ".y" ), config.getDouble( spawn
                        .getId() + ".z" ), ( float ) config.getInt( spawn.getId() + ".yaw" ), ( float ) config.getInt( spawn
                        .getId() + ".pitch" ) );
                spawn.setLocation( location );

                if ( spawn.getId().contains( "team" ) ) {

                    String[] strings = spawn.getId().split( Pattern.quote( "." ) );
                    for ( String string : strings ) {

                        if ( string.contains( "team" ) ) {

                            addTeamSpawn( string, spawn );
                        }
                    }
                }

                spawnList.add( spawn );

                MessageHandler.debug( "GamePhase (<phase>) -> Spawn loaded: <spawn>", "phase", gamePhase.getId(), "spawn", spawn
                        .getId() );
            }
        }
    }

    private void addTeamSpawn( String teamId, Spawn spawn ) {

        if ( spawn.getId().contains( "boss" ) ) {

            Team team = FightTheBoss.getInstance().getTeamHandler().getTeamById( teamId );
            team.setBossSpawn( spawn );

            return;
        }

        if ( teamSpawnMap.containsKey( teamId ) ) {
            List < Spawn > spawns = new LinkedList <>();
            spawns.addAll( teamSpawnMap.get( teamId ) );
            spawns.add( spawn );
            teamSpawnMap.put( teamId, spawns );

            return;
        }
        teamSpawnMap.put( teamId, Arrays.asList( spawn ) );
    }

    public Spawn getSpawnById( String id ) {

        if ( spawnList.size() == 0 ) {

            MessageHandler.debug( "No Spawns set yet." );
            return null;
        }

        for ( Spawn spawn : spawnList ) {

            if ( spawn.getId().contains( id ) ) {

                return spawn;
            }
        }

        MessageHandler.debug( "Spawn <spawn> not set yet.", "spawn", id );

        return getSpawnList().get( 0 );
    }

    public SpawnFactory copySpawnsFromExistingGamePhase( GamePhase gamePhase ) {

        setTeamSpawnMap( gamePhase.getSpawnFactory().getTeamSpawnMap() );
        spawnList = gamePhase.getSpawnFactory().getSpawnList();
        spawnList.forEach( spawn->MessageHandler.debug( "Copy -> Spawn (<spawn>)", "spawn", spawn.getId() ) );


        return this;
    }
}
