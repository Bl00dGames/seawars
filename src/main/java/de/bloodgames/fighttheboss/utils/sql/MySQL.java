package de.bloodgames.fighttheboss.utils.sql;

import lombok.Getter;
import lombok.Setter;

import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MySQL {

    @Getter
    private final String username, password;

    @Getter @Setter
    private String database, host;

    @Getter @Setter
    private int port;

    @Getter @Setter
    private Connection connection;

    @Getter
    private ExecutorService executorService = Executors.newCachedThreadPool();

    public MySQL( String username, String password ) {

        try {
            Class.forName( "com.mysql.jdbc.Driver" ).newInstance();
        } catch ( InstantiationException e ) {
            e.printStackTrace();
        } catch ( IllegalAccessException e ) {
            e.printStackTrace();
        } catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        }

        this.username = username;
        this.password = password;
    }

    public MySQL( String username, String password, String database, String host, int port ) {

        this( username, password );
        this.database = database;
        this.host = host;
        this.port = port;
    }

    public MySQL connectToDatabase( ) {

        if ( getConnection() != null ) {
            return this;
        }

        try {
            setConnection( DriverManager.getConnection( "jdbc:mysql://" + host + ":" + port + "/" + database, username, password ) );
        } catch ( SQLException e ) {
            System.err.println( "MySQL offline!" );
        }

        return this;
    }

    public MySQL close( ) {

        if ( getConnection() == null ) {
            return this;
        }
        try {
            getConnection().close();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        setConnection( null );

        return this;
    }

    public boolean isConnected( ) {

        return getConnection() != null;
    }

    public MySQL update( PreparedStatement preparedStatement, boolean async ) {

        connectToDatabase();

        if ( isConnected() ) {
            try {
                if ( async ) {

                    getExecutorService().execute( ( )->{
                        try {
                            if ( !preparedStatement.isClosed() ) {

                                preparedStatement.executeUpdate();
                            }
                        } catch ( SQLException e ) {
                            e.printStackTrace();
                        }
                    } );
                } else {

                    preparedStatement.executeUpdate();
                }
            } catch ( SQLException e ) {
                System.err.println( "SQLException: " + e.getMessage() );
                System.err.println( "SQLState: " + e.getSQLState() );
                System.err.println( "VendorError: " + e.getErrorCode() );
            }
        }

        return this;
    }

    public MySQL update( String table, String key, Object value, String whereKey, Object whereValue, boolean async ) {

        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement( "UPDATE " + table + " SET " + key + "='" + value + "' WHERE " + whereKey + "='" + whereValue + "'" );
            update( preparedStatement, async );
        } catch ( SQLException e ) {
            System.err.println( "SQLException: " + e.getMessage() );
            System.err.println( "SQLState: " + e.getSQLState() );
            System.err.println( "VendorError: " + e.getErrorCode() );
        }

        return this;
    }

    public MySQL update( String table, String key, Object value, boolean async ) {

        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement( "UPDATE " + table + " SET " + key + "='" + value + "'" );
            update( preparedStatement, async );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return this;
    }

    public ResultSet query( PreparedStatement preparedStatement, boolean async ) {

        connectToDatabase();

        final ResultSet[] resultSet = { null };

        if ( isConnected() ) {
            try {
                if ( async ) {

                    getExecutorService().execute( ( )->{
                        try {
                            resultSet[ 0 ] = preparedStatement.executeQuery();
                        } catch ( SQLException e ) {
                            e.printStackTrace();
                        }
                    } );
                } else {

                    resultSet[ 0 ] = preparedStatement.executeQuery();
                }
            } catch ( SQLException e ) {
                System.err.println( "SQLException: " + e.getMessage() );
                System.err.println( "SQLState: " + e.getSQLState() );
                System.err.println( "VendorError: " + e.getErrorCode() );
            }
        }

        return resultSet[ 0 ];
    }

    public boolean isInTable( String table, String whereKey, Object whereValue ) {

        boolean isInTable = false;

        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement( "SELECT * FROM `" + table + "` WHERE `" + whereKey + "`='" + whereValue + "'" );
            ResultSet resultSet = query( preparedStatement, false );
            if ( resultSet.next() ) {

                isInTable = true;
            }
        } catch ( SQLException e ) {
            System.err.println( "SQLException: " + e.getMessage() );
            System.err.println( "SQLState: " + e.getSQLState() );
            System.err.println( "VendorError: " + e.getErrorCode() );
        }

        return isInTable;
    }

    public Object get( String table, String whereKey, Object whereValue, String key ) {

        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement( "SELECT * FROM `" + table + "` WHERE `" + whereKey + "`='" + whereValue + "'" );
            ResultSet resultSet = query( preparedStatement, false );
            if ( resultSet.next() ) {

                return resultSet.getObject( key );
            }
        } catch ( SQLException e ) {
            System.err.println( "SQLException: " + e.getMessage() );
            System.err.println( "SQLState: " + e.getSQLState() );
            System.err.println( "VendorError: " + e.getErrorCode() );
        }

        return null;
    }

    public MySQL set( String table, String key, Object value, String whereKey, Object whereValue ) {

        update( table, key, value, whereKey, whereValue, true );

        return this;
    }

    public MySQL insert( String table, String tableKes, String values ) {

        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement( "INSERT INTO " + table + "(" + tableKes + ") VALUES (" + values + ")" );
            update( preparedStatement, true );
        } catch ( SQLException e ) {
            System.err.println( "SQLException: " + e.getMessage() );
            System.err.println( "SQLState: " + e.getSQLState() );
            System.err.println( "VendorError: " + e.getErrorCode() );
        }

        return this;
    }
}
