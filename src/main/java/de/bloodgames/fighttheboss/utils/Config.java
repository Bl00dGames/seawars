package de.bloodgames.fighttheboss.utils;

import lombok.Getter;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by André on 30.11.2017.
 */
public class Config extends YamlConfiguration {

    @Getter
    private final String path, name;
    @Getter
    private final File file;

    public Config( String path, String name ) {

        this.path = path;
        this.name = name;
        this.file = new File( path + "/" + name + ".yml" );

        //Creating file if not exists
        if ( getFile().exists() ) getFile().mkdirs();

        //Loading file
        try {
            load( getFile() );
        } catch ( IOException e ) {
            MessageHandler.debug( "File is not load yet. Loading File again." );
        } catch ( InvalidConfigurationException e ) {
            e.printStackTrace();
        }

        //Adding header to config
        options().header( name + " - Configuration" ).copyHeader( true ).copyHeader();

        //Saving file
        try {
            super.save( getFile() );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        MessageHandler.debug( "Config: <config> loaded successfully.", "config", getName() + ".yml" );
    }

    public Config addDefaultOption( String path, Object value ) {

        if ( get( path ) == null ) {

            set( path, value );
        }

        return this;
    }

    @Override
    public void set( String path, Object value ) {

        MessageHandler.debug( "Config: <config> set '<path>' to '<value>'.", "config", getName(), "path", path, "value", String
                .valueOf( value ) );
        super.set( path, value );
    }

    public void save( ) throws IOException {

        super.save( getFile() );
        MessageHandler.debug( "Config: <config> saved successfully.", "config", getName() + ".yml" );
    }
}
