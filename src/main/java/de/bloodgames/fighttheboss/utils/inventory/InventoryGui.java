package de.bloodgames.fighttheboss.utils.inventory;

import de.bloodgames.fighttheboss.FightTheBoss;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class InventoryGui implements Listener {

    @Getter
    private final String title;

    @Getter
    private final int size;

    @Getter
    private final Inventory inventory;

    @Getter
    @Setter
    private Player player;

    @Getter
    private ItemStack startItemStack;

    public InventoryGui( String title, int size ) {

        this.title = title;
        this.size = size;

        inventory = Bukkit.createInventory( null, size, ChatColor.translateAlternateColorCodes( '&', title ) );
        Bukkit.getPluginManager().registerEvents( this, FightTheBoss.getInstance() );
    }

    public InventoryGui( String title, int size, ItemStack startItemStack, Player player ) {

        this( title, size );

        this.startItemStack = startItemStack;
        setPlayer( player );
    }

    public InventoryGui openGui( Player player ) {

        setPlayer( player );

        initGui();

        player.openInventory( getInventory() );

        return this;
    }

    private void initGui( ) {


        if ( !FightTheBoss.getInstance().getUserFactory().getUser( getPlayer() ).getInventoryGuiList().contains( this ) ) {

            FightTheBoss.getInstance().getUserFactory().getUser( getPlayer() ).getInventoryGuiList().add( this );

            addItems();

            return;
        }

        updateItems();
    }

    public InventoryGui closeGui( ) {

        getPlayer().getOpenInventory().close();

        return this;
    }

    public abstract void addItems( );

    public abstract void updateItems( );

    public abstract void onClick( Player player, InventoryClickEvent clickEvent );

    @EventHandler
    public void onItemInteract( PlayerInteractEvent event ) {

        if ( event.getItem() == null || event.getItem().getType().equals( Material.AIR ) ) {
            return;
        }

        if ( !event.getItem().equals( getStartItemStack() ) ) {

            return;
        }

        if ( !event.getAction().equals( Action.RIGHT_CLICK_AIR ) && !event.getAction()
                .equals( Action.RIGHT_CLICK_BLOCK ) ) {

            return;
        }

        if ( event.getPlayer() != getPlayer() ) {
            return;
        }

        openGui( getPlayer() );
    }

    @EventHandler
    public void onInventoryClick( InventoryClickEvent event ) {

        Inventory inventory = event.getClickedInventory();

        if ( inventory == null || inventory.getTitle() == null || inventory.getTitle().equals( "" ) ) return;
        if ( !ChatColor.stripColor( inventory.getTitle() )
                .equals( ChatColor.stripColor( ChatColor.translateAlternateColorCodes( '&', getTitle() ) ) ) ) return;
        if ( getPlayer() == null ) return;
        if ( getPlayer() != event.getWhoClicked() ) return;

        event.setCancelled( true );
        onClick( ( ( Player ) event.getWhoClicked() ), event );
    }

    public void destroy( ) {

        closeGui();
        HandlerList.unregisterAll( this );
        setPlayer( null );
    }
}
