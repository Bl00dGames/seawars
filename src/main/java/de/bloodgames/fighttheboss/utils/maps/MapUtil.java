package de.bloodgames.fighttheboss.utils.maps;

import de.bloodgames.fighttheboss.utils.MessageHandler;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;
import java.io.IOException;

public class MapUtil {

    public static void copyMap( World world ) {

        File file = world.getWorldFolder();
        File targetFile = new File( file.getPath() + "temp" );

        try {
            FileUtils.copyDirectory( file, targetFile );
            MessageHandler.debug( "Map (<map>) is copied!", "map", world.getName() );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public static void restoreMap( World world ) {

        if ( Bukkit.getWorlds().contains( world ) ) {
            Bukkit.unloadWorld( world, false );
        }

        File file = new File( world.getWorldFolder() + "temp" );
        if ( !file.exists() ) return;
        File targetFile = world.getWorldFolder();

        try {
            FileUtils.copyDirectory( file, targetFile );
            FileUtils.deleteDirectory( file );

            MessageHandler.debug( "Map (<map>) is restored!", "map", world.getName() );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
