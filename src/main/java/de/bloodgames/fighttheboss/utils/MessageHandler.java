package de.bloodgames.fighttheboss.utils;

import de.bloodgames.fighttheboss.FightTheBoss;
import de.bloodgames.fighttheboss.usermanagement.User;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by André on 29.11.2017.
 */
public class MessageHandler {

    public static void debug( String message, String... variables ) {

        if ( FightTheBoss.getInstance().getGameFactory() != null ) {

            if ( !FightTheBoss.getInstance().getGameFactory().isDebug() ) {
                return;
            }
        }

        String finalMessage = format( MessageType.DEBUG.getPrefix() + " " + message, variables );

        Logger.getLogger( "debug" ).info( ChatColor.stripColor( finalMessage ) );

        return;
    }

    public static String getMessage( String message, String... variables ) {

        return format( message, variables );
    }

    public static String getMessage( MessageType messageType, String message, String... variables ) {

        String finalMessage = format( messageType.getPrefix() + " " + message, variables );
        if ( messageType == MessageType.GAME )
            finalMessage = format( finalMessage, "game", FightTheBoss.getInstance().getGameFactory().getPrefix() );
        return finalMessage;
    }

    private static String format( String message, String... variables ) {

        Map < String, String > variableMap = new HashMap <>();

        if ( variables.length % 2 == 0 ) {
            for ( int counter = 0; counter < variables.length; counter += 2 ) {
                variableMap.put( variables[ counter ], variables[ counter + 1 ] );
            }
        }

        //german language
        variableMap.put( "ae", "ä" );
        variableMap.put( "AE", "Ä" );
        variableMap.put( "ue", "ü" );
        variableMap.put( "UE", "Ü" );
        variableMap.put( "oe", "ö" );
        variableMap.put( "OE", "Ö" );
        variableMap.put( "ss", "ß" );

        variableMap.put( "n", "\n" );
        variableMap.put( "ng", "\n" + MessageType.GAME.getPrefix() );

        for ( Map.Entry < String, String > entry : variableMap.entrySet() ) {
            message = message.replaceAll( "\\<" + entry.getKey() + "\\>", entry.getValue() );
        }

        return ChatColor.translateAlternateColorCodes( '&', message );
    }

    public MessageHandler sendMessage( MessageType messageType, User user, Class classId, String message, String... variables ) {

        if ( message == null || message.equalsIgnoreCase( "null" ) ) {

            debug( "Message not set yet! [<type>,<user>,<class>]", "type", messageType.getPrefix(), "user", ( user == null ? "null" : user
                    .getUsername() ), "class", classId.toString() );
            return this;
        }

        String finalMessage = format( messageType.getPrefix() + " " + message, variables );

        if ( messageType == MessageType.USER ) finalMessage = format( finalMessage, "player", user.getUsername() );
        if ( messageType == MessageType.GAME )
            finalMessage = format( finalMessage, "game", FightTheBoss.getInstance().getGameFactory().getPrefix() );
        if ( messageType == MessageType.DEBUG ) {

            debug( finalMessage );
            return this;
        }

        Bukkit.broadcastMessage( finalMessage );

        return this;
    }


    public enum MessageType {

        DEBUG( "debug", "§7[§4Debug§7]" ),
        SERVER( "server", "§7[§4Server§7]" ),
        GAME( "game", "§7[§c<game>§7]" ),
        USER( "user", "§8<player>§7:§f" ),
        NULL( "null", "" );

        @Getter
        private String messageTypeId;
        @Getter
        private String prefix;

        MessageType( String messageTypeId, String prefix ) {

            this.messageTypeId = messageTypeId;
            this.prefix = prefix;
        }
    }
}
