package de.bloodgames.fighttheboss.utils.items;

import de.bloodgames.fighttheboss.utils.MessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemBuilder {

    @Getter
    @Setter
    private ItemStack itemStack;
    @Getter
    @Setter
    private ItemMeta itemMeta;
    @Getter
    @Setter
    private Material material;

    @Getter
    private List < String > loreList = new ArrayList <>();

    public ItemBuilder( Material material ) {

        setMaterial( material );
        setItemStack( new ItemStack( material ));
        setItemMeta( getItemStack().getItemMeta() );
    }

    public ItemBuilder( ) {

    }

    ;

    public ItemBuilder fromItemStack( ItemStack itemStack ) {

        setItemStack( itemStack);
        setItemMeta( getItemStack().getItemMeta() );
        setMaterial( getItemStack().getType() );

        return this;
    }

    public ItemBuilder setName( String name ) {

        getItemMeta().setDisplayName( ChatColor.translateAlternateColorCodes( '&', name ) );

        return this;
    }

    public ItemBuilder setAmount( int amount ) {

        getItemStack().setAmount( amount );

        return this;
    }

    public ItemBuilder setDurability( final short durability ) {

        getItemStack().setDurability( durability );

        return this;
    }

    public ItemBuilder addEnchantment( Enchantment enchantment, int level ) {

        getItemMeta().addEnchant( enchantment, level, true );

        return this;
    }

    public ItemBuilder removeEnchantment( Enchantment enchantment ) {

        getItemMeta().removeEnchant( enchantment );

        return this;
    }

    public ItemBuilder addLore( String lore ) {

        lore = ChatColor.translateAlternateColorCodes( '&', lore );

        getLoreList().add( ChatColor.GRAY + lore );

        return this;
    }

    public ItemBuilder hideFlags( ) {

        getItemMeta().addItemFlags( ItemFlag.HIDE_ATTRIBUTES );
        getItemMeta().addItemFlags( ItemFlag.HIDE_DESTROYS );
        getItemMeta().addItemFlags( ItemFlag.HIDE_ENCHANTS );
        getItemMeta().addItemFlags( ItemFlag.HIDE_PLACED_ON );
        getItemMeta().addItemFlags( ItemFlag.HIDE_POTION_EFFECTS );
        getItemMeta().addItemFlags( ItemFlag.HIDE_UNBREAKABLE );

        return this;
    }

    public ItemBuilder setColor( Color color ) {

        if ( !getItemStack().getType().name().contains( "LEATHER_" ) ) {

            MessageHandler.debug( "Method leatherColor() only available for leather armor!" );
            return this;
        }

        LeatherArmorMeta leatherArmorMeta = ( ( LeatherArmorMeta ) getItemMeta() );
        leatherArmorMeta.setColor( color );

        return this;
    }

    public ItemBuilder setSkullOwner( String owner ) {

        if ( getItemStack().getType() != Material.SKULL_ITEM ) {

            MessageHandler.debug( "Method setSkullOwner() only available for skull items!" );
            return this;
        }

        setDurability( ( ( short ) SkullType.PLAYER.ordinal() ) );
        SkullMeta skullMeta = ( ( SkullMeta ) getItemMeta() );
        skullMeta.setOwningPlayer( Bukkit.getOfflinePlayer( owner ) );

        return this;
    }

    public ItemBuilder setUnbreakable( boolean value ) {

        getItemMeta().spigot().setUnbreakable( value );

        return this;
    }

    public ItemBuilder addEffectToGlow( ) {

        getItemMeta().addEnchant( Enchantment.DURABILITY, 1, true );

        return this;
    }

    public ItemStack build( ) {

        apply();

        return getItemStack();
    }

    public ItemBuilder apply( ) {

        getItemMeta().setLore( getLoreList() );
        getItemStack().setItemMeta( getItemMeta() );

        return this;
    }
}
