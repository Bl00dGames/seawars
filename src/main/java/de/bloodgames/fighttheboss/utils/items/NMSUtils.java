package de.bloodgames.fighttheboss.utils.items;

import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;

import java.lang.reflect.Field;
import java.util.List;

/*
* An Utility class for net.minecraft.server related operations, mostly mobs and items.
* NO COPYRIGHT. Use it however you feel like. You can claim it as your own, you can modify it,
* you can share it, you can use it, with no permission whatsoever, I don't mind :)
* @author jetp250
*/
public final class NMSUtils {

    private static final Field META_LIST_MONSTER = null;
    private static final Field META_LIST_CREATURE = null;
    private static final Field META_LIST_AMBIENT = null;
    private static final Field META_LIST_WATER_CREATURE = null;
    /**
     * a CraftBukkit server field to reduce continuous casting from
     * {@link Bukkit#getServer()}.
     */
    public static final CraftServer SERVER = null;

    private static boolean accessible;

    private static boolean isAccessible( ) {

        return accessible;
    }

    private static void init( ) {

        accessible = !( META_LIST_MONSTER == null || META_LIST_CREATURE == null || META_LIST_AMBIENT == null
                || META_LIST_WATER_CREATURE == null );
    }

    /**
     * Registers the custom class to be available for use.
     *
     * @param name        - The 'savegame id' of the mob.
     * @param type        - The type of your mob
     * @param customClass - Your custom class that'll be used
     * @param biomes      - Should your mob be set as a default in each biome? Only one
     *                    custom entity of this type entity can have this set as 'true'.
     * @see EntityType#getName() EntityType#getName() for the savegame id.
     */
    @SuppressWarnings( "unchecked" )
    public static void registerEntity( final String name, final EntityType type,
                                       final Class < ? extends Entity > customClass, boolean biomes ) {

        final MinecraftKey key = new MinecraftKey( name );
        EntityTypes.b.a( type.getId(), key, customClass );
        if ( !EntityTypes.d.contains( key ) ) {
            EntityTypes.d.add( key );
        }
        if ( !isAccessible() || !biomes) {
            return;
        }
        final Field field;
        if ( ( field = type.getMeta().getField() ) == null ) {
            return;
        }
        try {
            field.setAccessible( true );
            for ( Object base : BiomeBase.i ) {
                final List < BiomeBase.BiomeMeta > list = ( List < BiomeBase.BiomeMeta > ) field.get( base );
                for ( BiomeBase.BiomeMeta meta : list ) {
                    if ( meta.b == type.getNMSClass() ) {
                        meta.b = ( Class < ? extends EntityInsentient > ) customClass;
                        break;
                    }
                }
            }
            field.setAccessible( false );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public enum MobType implements EntityType {

        ZOMBIE( 54, "zombie", EntityZombie.class, MobMeta.MONSTER );

        private final int id;
        private final String name;
        private final Class < ? extends EntityInsentient > clazz;
        private final MobMeta meta;

        private MobType( final int id, final String name, Class < ? extends EntityInsentient > nmsClazz,
                         final MobMeta meta ) {

            this.id = id;
            this.name = name;
            this.clazz = nmsClazz;
            this.meta = meta;
        }

        @Override
        public MobMeta getMeta( ) {

            return meta;
        }

        @Override
        public int getId( ) {

            return id;
        }

        @Override
        public String getName( ) {

            return name;
        }

        @Override
        public Class < ? extends EntityInsentient > getNMSClass( ) {

            return clazz;
        }
    }

    private interface EntityType {

        /**
         * Returns the mob's network hexadecimal id value. Used to tell the
         * client which mob should be rendered.
         *
         * @return The ID as an int.
         */
        int getId( );

        /**
         * Rather than returning its name, this returns the mob's
         * <b>savegame-id</b>. A Custom savegame-id can be provided instead of
         * this, though.
         *
         * @return The savegame-id String.
         */
        String getName( );

        /**
         * Returns the NMS class of this EntityType. Used when overriding the
         * mob to spawn as a default in biomes.
         *
         * @return The NMS class your mob should be extending.
         */
        Class < ? extends Entity > getNMSClass( );

        /**
         * Returns the meta containing info about the list field in which the
         * mob will be added to.
         */
        MobMeta getMeta( );
    }

    public enum MobMeta {
        MONSTER( META_LIST_MONSTER );

        private final Field field;

        private MobMeta( final Field field ) {

            this.field = field;
        }

        /**
         * @return the BiomeMeta list field of this entity.
         * <p>
         * <b>Undefined will not be accepted and returns null.</b>
         * </p>
         */
        public Field getField( ) {

            return field;
        }
    }
}
