# FightTheBoss

Dies ist das Spiel FightTheBoss, lediglich unter dem Link /seawars/ bezüglich der Bewerbung, nachträgliche Änderung.

Bei diesem Spiel kämpfen zwei Teams gegeneinander, deren Ziel es ist den gegnerischen Boss zu töten.
Waffen können derzeit über Kisten, ähnlich wie bei TTT, erhalten werden.
Des Weiteren gibt es nach ablauf der Zeit ein DeathMatch.

Das Spiel ist beendet, wenn der König, sowie alle Spieler eines Teams gestorben sind, oder nur noch ein Team im DeathMatch lebt.

